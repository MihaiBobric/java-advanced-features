package com.sda.controllers;

import com.sda.entities.CategoryEntity;
import com.sda.repositories.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
public class CategoryController {
    @Autowired
    private CategoryRepository categoryRepository;

    @GetMapping("/category/list")
    public ModelAndView getCategories() {
        ModelAndView modelAndView = new ModelAndView("categories");
        modelAndView.addObject("categoryList", categoryRepository.findAll());
        return modelAndView;
    }

    @GetMapping("/category/add")
    public ModelAndView addCategory() {
        ModelAndView modelAndView = new ModelAndView("category-form");
        modelAndView.addObject("category", new CategoryEntity());
        return modelAndView;
    }

    @PostMapping("/category/save")
    public ModelAndView saveCategory(@ModelAttribute("category") @Valid CategoryEntity categoryEntity, BindingResult bindingResult) {
        ModelAndView modelAndView = new ModelAndView("redirect:/category/list");
        if (bindingResult.hasErrors()) {
            modelAndView.setViewName("category-form");
            modelAndView.addObject("category", categoryEntity);
            return modelAndView;
        }
        categoryRepository.save(categoryEntity);
        return modelAndView;
    }
    @GetMapping("/category/edit/{id}")
    public ModelAndView editCategory(@PathVariable Integer id) {
        ModelAndView modelAndView = new ModelAndView("category-form");
        modelAndView.addObject("category", categoryRepository.findById(id).get());
        return modelAndView;
    }
    @GetMapping("/category/delete/{id}")
    public ModelAndView deleteCategory(@PathVariable Integer id) {
        ModelAndView modelAndView = new ModelAndView("redirect:/category/list");
        categoryRepository.deleteById(id);
        return modelAndView;
    }

}
