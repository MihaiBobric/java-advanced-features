package com.sda.controllers;

import com.sda.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ProductController {
    @Autowired
    private ProductRepository productRepository;

    @GetMapping("/")
    public ModelAndView getIndex(){
        ModelAndView modelAndView=new ModelAndView("index");
        return modelAndView;
    }
}
