package com.sda.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();
        http.authorizeRequests()
                .antMatchers("/", "/login")
                .permitAll();
        http.authorizeRequests() //.and()
        .antMatchers("/category/edit/**","/category/delete/**")
                .hasRole("ADMIN");
        http.authorizeRequests() //.and()
                .anyRequest()
                .authenticated();
        http.formLogin()
                .loginProcessingUrl("/login")
                .loginPage("/")
                .usernameParameter("username")
                .passwordParameter("password")
                .defaultSuccessUrl("/category/list")
                .failureUrl("/");
        http.logout()
                .logoutUrl("/logout")
                .permitAll();

    }
    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception{
        PasswordEncoder passwordEncoder=new BCryptPasswordEncoder();
        auth.inMemoryAuthentication()
                .withUser("Bobi")
                .password(passwordEncoder.encode("1234"))
                .roles("USER")
                .and()
                .withUser("BobiBoi")
                .password(passwordEncoder.encode("12345"))
                .roles("USER","ADMIN")
                .and()
                .passwordEncoder(passwordEncoder);
    }

}
