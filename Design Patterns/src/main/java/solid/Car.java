package solid;

public interface Car {
    void start();
    void accelerate(int kph);
    void stop();

}

