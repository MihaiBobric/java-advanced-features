package solid;

public class Main {

    public static Car car;
    public static void drive(Car car){
        car.start();
        car.accelerate(10);
        car.stop();
    }

    public static void main(String[] args) {
        System.out.println("R");

        car=new MotorCar();
        Car electricCar = new ElectricCar();
        drive(car);
        System.out.println("*******************");
        drive(electricCar);
        System.out.println("*******************");
        ElectricCar specificCar= new ElectricCar();
        specificDrive(specificCar);
        System.out.println("*******************");
        drive(specificCar);

    }
    public static void specificDrive(ElectricCar electricCar){
        electricCar.start();
        electricCar.accelerate(45);
        electricCar.stop();
        electricCar.regenerativeBraking();
    }


}
