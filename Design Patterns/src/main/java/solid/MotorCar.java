package solid;

public class MotorCar implements Car{
    private Engine engine;

    public MotorCar(){
        this.engine=new Engine();
    }
    public void start() {
        System.out.println("Diesel car");
        engine.run();
    }

    public void accelerate(int kph) {
        System.out.println("Gas");
        engine.speed(kph);
    }

    public void stop() {
        System.out.println("Hidraulic brake");
        engine.stop();
    }


}
