package solid;

public class Engine {
    public void run(){
        System.out.println("Engine is running;");
    }
    public void stop(){
        System.out.println("Engine is not running;");
    }
    public void speed(int speed){
        System.out.println("Speed is "+ speed+";");
    }
}
