package solid;

public class ElectricCar implements Car {
    private Engine engine;

    public ElectricCar() {
        this.engine = new Engine();
    }
    public void start() {
        System.out.println("Battery car");
        engine.run();
    }

    public void accelerate(int kph) {
        System.out.println("Voltage");
        engine.speed(kph);
    }

    public void stop() {
        System.out.println("Electric brakes");
        engine.stop();
    }
    public void regenerativeBraking(){
        engine.stop();
        System.out.println("Regenerating battery");
    }

}
