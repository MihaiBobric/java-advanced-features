package fluentInterface;

import java.util.ArrayList;
import java.util.List;

public class FoodMenu implements Menu{
    List<Pizza> pizzaList;
    List<String> selectedPizza;

    public FoodMenu() {
        this.pizzaList = new ArrayList<Pizza>();
        this.selectedPizza=new ArrayList<String>();
        populateMenu();

    }

    private void populateMenu() {
        pizzaList.add(new VeganPizza());
    }

    public Menu orderPizza(List<String> orders) {
        for(String order:orders){
            selectedPizza.add(order);
            System.out.println("Ordered " + order);
        }
        return this;
    }

    public Menu eatPizza() {
        System.out.println("Ate pizza");
        return this;
    }

    public Menu payPizza() {
        System.out.println("Paid for pizza");
        return this;
    }
}
