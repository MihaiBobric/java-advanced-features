package designPatterns.singleton;

public class Logging {
    private String lastMessage;
    private static Logging instance=null;

    private Logging(){
        lastMessage=new String();
    }
    public static Logging getInstance(){
        if (instance==null){
            instance=new Logging();
        }
        return instance;
    }
    public void printMessage(String message){
        System.out.println(message);
        this.lastMessage=message;
    }

    @Override
    public String toString() {
        return "Logging{" +
                "lastMessage='" + lastMessage + '\'' +
                '}';
    }
}
