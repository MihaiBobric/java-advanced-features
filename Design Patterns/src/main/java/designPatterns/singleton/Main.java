package designPatterns.singleton;

public class Main {
    public static void main(String[] args) {
        Logging l1=Logging.getInstance();//=new Logging();
        l1.printMessage("created service");//l1.lastMessage="..."

        Logging l2=Logging.getInstance();//
        System.out.println(l1);
        l2.printMessage("created new service");
        System.out.println(l1);


    }
}
