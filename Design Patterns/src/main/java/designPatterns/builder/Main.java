package designPatterns.builder;

public class Main {
    public static void main(String[] args) {
        Home.HomeBuilder mansionBuilder=new Home.HomeBuilder("Beton", "Lemn", "Sarpanta");
        Home basicHome=mansionBuilder.build();

        Home.HomeBuilder castleBuilder=new Home.HomeBuilder("Beton","Caramida","Sarpanta");
        Home complexHome=castleBuilder.addBasement("Demisol").addRooms(5).build();
        System.out.println(basicHome);
        System.out.println(complexHome);

    }
}
