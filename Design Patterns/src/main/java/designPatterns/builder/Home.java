package designPatterns.builder;

public class Home {
    private String foundation;
    private String basement;
    private String walls;
    private String roof;
    private int roomNumber;

//    public void setFoundation(String foundation) {
//        this.foundation = foundation;
//    }
//
//    public void setBasement(String basement) {
//        this.basement = basement;
//    }
//
//    public void setWalls(String walls) {
//        this.walls = walls;
//    }
//
//    public void setRoof(String roof) {
//        this.roof = roof;
//    }
//
//    public void setRoomNumber(int roomNumber) {
//        this.roomNumber = roomNumber;
//    }


    @Override
    public String toString() {
        return "Home{" +
                "foundation='" + foundation + '\'' +
                ", basement='" + basement + '\'' +
                ", walls='" + walls + '\'' +
                ", roof='" + roof + '\'' +
                ", roomNumber=" + roomNumber +
                '}';
    }

    public static class HomeBuilder{
    private Home home;

        public HomeBuilder(String foundation, String walls, String roof) {
            this.home = new Home();
            home.foundation=foundation;
            home.walls=walls;
            home.roof=roof;
        }

        public HomeBuilder addBasement(String basement){
            home.basement=basement;
            return this;
        }

        public HomeBuilder addRooms(int roomNumber){
            home.roomNumber=roomNumber;
            return this;
        }

        public Home build(){
            return home;
        }
    }




}


