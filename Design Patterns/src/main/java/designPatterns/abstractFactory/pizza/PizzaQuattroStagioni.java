package designPatterns.abstractFactory.pizza;

public class PizzaQuattroStagioni extends   Pizza{
    private int size;

    public PizzaQuattroStagioni(int size) {
        this.size = size;
    }

    public String getName() {
        return PizzaType.QUATTROSTAGIONI.toString();
    }

    public String getIngredients() {
        return "Vegan mozzarella, Champignon, Olives, Soy, Spinach, Tomato sauce, Basil";
    }

    public int getSize() {
        return size;
    }
}

