package designPatterns.abstractFactory.pizza;

public class PizzaMargherita extends Pizza{
    private int size;

    public PizzaMargherita(int size){
        this.size=size;
    }

    public String getName() {
        return PizzaType.MARGHERITE.toString();
    }

    public String getIngredients() {
        return "Vegan mozzarella, Tomato sauce, Basil";
    }

    public int getSize() {
        return size;
    }
}
