package designPatterns.abstractFactory.pizza;

public abstract class Pizza {
    abstract String getName();
    abstract String getIngredients();
    abstract int getSize();

    @Override
    public String toString() {
        return getName() +" " +getIngredients()+" "+getSize();
    }
}
