package designPatterns.abstractFactory.pizza;

public class PizzaQuattroFormaggi extends Pizza{
    private int size;

    public PizzaQuattroFormaggi(int size){
        this.size=size;
    }

    public String getName() {
        return PizzaType.QUATTROFORMAGGI.toString();
    }

    public String getIngredients() {
        return "Vegan mozzarella, Vegan Cheddar, Vegan Slice, Vegan Feta, Tomato sauce, Basil";
    }

    public int getSize() {
        return size;
    }
}


