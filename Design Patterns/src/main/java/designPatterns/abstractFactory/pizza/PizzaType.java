package designPatterns.abstractFactory.pizza;

public enum PizzaType {
    QUATTROSTAGIONI,
    MARGHERITE,
    QUATTROFORMAGGI
}

