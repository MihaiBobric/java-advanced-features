package designPatterns.abstractFactory;

import designPatterns.abstractFactory.factories.AbstractFactory;
import designPatterns.abstractFactory.factories.PizzaFactory;
import designPatterns.abstractFactory.pizza.Pizza;
import designPatterns.abstractFactory.pizza.PizzaType;

public class Main {
    public static void main(String[] args) {
        AbstractFactory pizzaFactory=new PizzaFactory();
        Pizza comanda1=pizzaFactory.create(PizzaType.QUATTROFORMAGGI,30);
        Pizza comanda2=pizzaFactory.create(PizzaType.MARGHERITE,40);
        Pizza comanda3=pizzaFactory.create(PizzaType.QUATTROSTAGIONI,30);
        System.out.println(comanda1);
        System.out.println(comanda2);
        System.out.println(comanda3);
    }
}
