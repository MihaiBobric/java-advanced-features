package designPatterns.abstractFactory.factories;

import designPatterns.abstractFactory.pizza.Pizza;
import designPatterns.abstractFactory.pizza.PizzaType;

public interface AbstractFactory {
    Pizza create (PizzaType type,int size);

}



