package designPatterns.abstractFactory.factories;

import designPatterns.abstractFactory.pizza.*;

public class PizzaFactory implements AbstractFactory{

    public Pizza create(PizzaType type,int size) {
        switch (type) {
            case MARGHERITE:
                return new PizzaMargherita(size);
            case QUATTROFORMAGGI:
                return new PizzaQuattroFormaggi(size);
            case QUATTROSTAGIONI:
                return new PizzaQuattroStagioni(size);
            default: return null;
        }
    }
}
