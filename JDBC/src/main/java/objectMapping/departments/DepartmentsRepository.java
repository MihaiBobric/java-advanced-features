package objectMapping.departments;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DepartmentsRepository {
    public void findAll(){
        try(
            Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/exercitiu?user=root&password=Ralulove&serverTimezone=UTC&characterEncoding=utf8");
            PreparedStatement prepStat = conn.prepareStatement("SELECT * FROM departments")){
                ResultSet rs = null;
                rs = prepStat.executeQuery();
                List<DepartmentsEntity> orders = new ArrayList<DepartmentsEntity>();
                while(rs.next()){
                    orders.add(new DepartmentsEntity(rs.getInt(1),rs.getString(2)));
                }
                System.out.println(orders.toString());

            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
    }
    public void findByld(int departmentId) {
        try(
                Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/exercitiu?user=root&password=Ralulove&serverTimezone=UTC&characterEncoding=utf8");
                PreparedStatement prepStat = conn.prepareStatement("SELECT name FROM departments WHERE departmentId= ?")){
            prepStat.setInt(1, departmentId);
            ResultSet rs = null;
            rs = prepStat.executeQuery();
            List<DepartmentsEntity> orders = new ArrayList<DepartmentsEntity>();
            while(rs.next()){
                orders.add(new DepartmentsEntity(rs.getString(1)));
            }
            System.out.println(orders.toString());

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

}
