package objectMapping.departments;

public class DepartmentsEntity {
    private int departmentId;
    private String name;

    public int getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(int departmentId) {
        this.departmentId = departmentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public DepartmentsEntity(int departmentId, String name) {
        this.departmentId = departmentId;
        this.name = name;
    }
    public DepartmentsEntity( String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "DepartmentsEntity{" +
                "departmentId=" + departmentId +
                ", name='" + name + '\'' +
                '}';
    }
}
