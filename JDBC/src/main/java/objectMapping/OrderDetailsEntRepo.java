package objectMapping;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class OrderDetailsEntRepo {
    public void findAll() {
        try(
                Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/classicmodels?user=root&password=Ralulove&serverTimezone=UTC&characterEncoding=utf8");
                PreparedStatement prepStat = conn.prepareStatement("SELECT * FROM orderDetails")){
            ResultSet rs = null;
            rs = prepStat.executeQuery();
            List<OrderDetailsEnt> orders = new ArrayList<OrderDetailsEnt>();
            while(rs.next()){
                orders.add(new OrderDetailsEnt(rs.getInt(1),rs.getString(2),rs.getInt(3),rs.getDouble(4),rs.getInt(5)));
            }
            System.out.println(orders.toString());

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}
