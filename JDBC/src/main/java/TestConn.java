import java.sql.*;

public class TestConn {
    public static void main(String[] args) {
        Connection con = null;
        try{
//            Class.forName("com.mysql.jdbc.Driver");
            con=DriverManager.getConnection("jdbc:mysql://localhost:3306/classicmodels?user=root&password=Ralulove" +
                    "&serverTimezone=UTC&characterEncoding=utf8");
            System.out.println("Connection success");

            Statement statement=con.createStatement();
            ResultSet resultSet=statement.executeQuery("SELECT * FROM employees");
            while (resultSet.next()) {
                Integer empId = resultSet.getInt("employeeNumber");
                String lastN = resultSet.getString("lastName");
                String firstN = resultSet.getString("firstName");
                String ext = resultSet.getString("extension");
                String job = resultSet.getString("jobTitle");
                System.out.println(empId+" "+lastN+" "+firstN+" "+ext+" "+job);
            }
            resultSet.close();
            statement.close();
            con.close();
        }catch(Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
