package com.crud.jdbc;

import java.sql.*;

public class EmployeesRepository {
    public void selectEmployees(){
        String query1="SELECT employeeId , firstName , lastName , dateOfBirth FROM employees";
        String query2="SELECT employeeId , firstName , lastName , dateOfBirth FROM employees WHERE lastName LIKE 'J%'";
        String query3="SELECT employeeId , firstName , lastName , dateOfBirth FROM employees WHERE departmentId IS NULL";
        String query4="SELECT employeeId , firstName , lastName , dateOfBirth FROM employees " +
                "JOIN departments ON employees.departmentId=departments.departmentId";

        try (Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/exercitiu?user=root&password=Ralulove&serverTimezone=UTC&characterEncoding=utf8");
             PreparedStatement statement1=con.prepareStatement(query1);
             ResultSet resultSet1=statement1.executeQuery();
             PreparedStatement statement2=con.prepareStatement(query2);
             ResultSet resultSet2=statement2.executeQuery();
             PreparedStatement statement3=con.prepareStatement(query3);
             ResultSet resultSet3=statement3.executeQuery();
             PreparedStatement statement4=con.prepareStatement(query4);
             ResultSet resultSet4=statement4.executeQuery()){

            System.out.println();
            while (resultSet1.next()) {
                int employeeId=resultSet1.getInt("employeeId");
                String dateOfBirth=resultSet1.getString("dateOfBirth");
                String firstName=resultSet1.getString("firstName");
                String lastName=resultSet1.getString("lastName");
                System.out.println(employeeId+" "+firstName+" "+lastName+" "+dateOfBirth);
            }
            System.out.println();
            while (resultSet2.next()) {
                int employeeId=resultSet2.getInt("employeeId");
                String dateOfBirth=resultSet2.getString("dateOfBirth");
                String firstName=resultSet2.getString("firstName");
                String lastName=resultSet2.getString("lastName");
                System.out.println(employeeId+" "+firstName+" "+lastName+" "+dateOfBirth);
            }
            System.out.println();
            while (resultSet3.next()) {
                int employeeId=resultSet3.getInt("employeeId");
                String dateOfBirth=resultSet3.getString("dateOfBirth");
                String firstName=resultSet3.getString("firstName");
                String lastName=resultSet3.getString("lastName");
                System.out.println(employeeId+" "+firstName+" "+lastName+" "+dateOfBirth);
            }
            System.out.println();
            while (resultSet4.next()) {
                int employeeId=resultSet4.getInt("employeeId");
                String dateOfBirth=resultSet4.getString("dateOfBirth");
                String firstName=resultSet4.getString("firstName");
                String lastName=resultSet4.getString("lastName");
                System.out.println(employeeId+" "+firstName+" "+lastName+" "+dateOfBirth);
            }


        }catch(SQLException throwables){
            throwables.printStackTrace();
        }
    }
}
