package com.crud.jdbc;

import java.sql.*;

public class CrudUtils {
    public void selectProduct(double a , double b){
        Connection con = null;
        try{
//            Class.forName("com.mysql.cj.jdbc.Driver");
            con= DriverManager.getConnection("jdbc:mysql://localhost:3306/classicmodels?user=root&password=Ralulove" +
                    "&serverTimezone=UTC&characterEncoding=utf8");
            System.out.println("Connection success");
            ResultSet resultSet=null;
            PreparedStatement statement=con.prepareStatement("SELECT * FROM products WHERE buyPrice BETWEEN ? AND ?");
            statement.setDouble(1, a);
            statement.setDouble(2, b);
            resultSet=statement.executeQuery();
            while (resultSet.next()) {
                double buyprice = resultSet.getDouble("buyPrice");

                System.out.println(buyprice);
            }
        }catch (Exception e){
           e.printStackTrace();
        }
    }

//public void selectProduct(double a, double b) {
//    //partea de logare
//    Connection con = null;
//    try {
//        //ne conectam la bd
////        Class.forName("com.mysql.cj.jdbc.Driver");
//        con = DriverManager.getConnection("jdbc:mysql://localhost:3306/classicmodels?user=root&password=Ralulove&serverTimezone=UTC&characterEncoding=utf8");
//        System.out.println("Connection succesful");
//        ResultSet rs = null;
//        PreparedStatement stmt = con.prepareStatement("SELECT * FROM products WHERE buyPrice BETWEEN ? AND ?");
//        stmt.setDouble(1, a);
//        stmt.setDouble(2,b);
//        rs =stmt.executeQuery();
//        while(rs.next()){
//            double buyPrice = rs.getDouble("buyPrice");
//            System.out.println("Pret vanzare in intervalul dorit de noi:" + buyPrice);
//        }
//    } catch (Exception e) {
//        e.printStackTrace();
//    }
//}

public void updateProduct(String a,String b) {
    Connection con = null;
    try {

        con = DriverManager.getConnection("jdbc:mysql://localhost:3306/classicmodels?user=root&password=Ralulove&serverTimezone=UTC&characterEncoding=utf8");
        System.out.println("Connection succesful");
        ResultSet rs = null;
        PreparedStatement stmt = con.prepareStatement("UPDATE products SET productName=? WHERE productCODE=? ");
        stmt.setString(1, a);
        stmt.setString(2, b);

        int size =stmt.executeUpdate(); //executeUpdate INSERT UPDATE DELETE
        stmt.close();
        if (size>0){
            System.out.println("Updated "+size);
        }

        } catch (Exception e) {
        e.printStackTrace();
    }
}

public void insertProduct(String a,String b, String c, String d, String e,String f, String g, String h,String i){
    Connection con = null;
    try {
        con = DriverManager.getConnection("jdbc:mysql://localhost:3306/classicmodels?user=root&password=Ralulove&serverTimezone=UTC&characterEncoding=utf8");
        System.out.println("Connection succesful");
        ResultSet rs = null;
        PreparedStatement stmt = con.prepareStatement("INSERT into classicmodels.products VALUES(?,?,?,?,?,?,?,?,?) ");
        stmt.setString(1, a);
        stmt.setString(2, b);
        stmt.setString(3, c);
        stmt.setString(4, d);
        stmt.setString(5, e);
        stmt.setString(6, f);
        stmt.setString(7, g);
        stmt.setString(8, h);
        stmt.setString(9, i);

        int size =stmt.executeUpdate(); //executeUpdate INSERT UPDATE DELETE
        stmt.close();
        if (size>0){
            System.out.println("Inserted "+size);
        }

    } catch (Exception ex) {
        ex.printStackTrace();
    }
}

public boolean deleteProduct(String code) {
    String query = "DELETE FROM products WHERE productCode=? ";
    try (Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/classicmodels?user=root&password=Ralulove&serverTimezone=UTC&characterEncoding=utf8");
         PreparedStatement stmt = con.prepareStatement(query)){

        stmt.setString(1, code);

        int size =stmt.executeUpdate(); //executeUpdate INSERT UPDATE DELETE
        if (size>0) return true;

    }catch (SQLException throwables){
    throwables.printStackTrace();
    }
    return false;
    }

}
