package com.crud.jdbc;

import java.sql.*;

public class ProjectsRepository {
    public void selectProjects(){
        String query="SELECT * FROM projects";
        try (Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/exercitiu?user=root&password=Ralulove&serverTimezone=UTC&characterEncoding=utf8");
             PreparedStatement statement=con.prepareStatement(query);
             ResultSet resultSet=statement.executeQuery()){

            while (resultSet.next()) {
                int projectId=resultSet.getInt("projectId");
                String description=resultSet.getString("description");
                System.out.println(projectId+" "+description);
            }

        }catch(SQLException throwables){
            throwables.printStackTrace();
        }
    }
}
