package com.crud.jdbc.hibernate;

import javax.persistence.*;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Table(name="alien")
public class Alien {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "alienId")
    private int alienId;
    @Column(name = "name")
    private String name;
    @Column(name = "type")
    private String type;
    @Column(name = "color")
    private String color;
    public Alien(){};
    public Alien( String name, String type, String color) {
        this.name = name;
        this.type = type;
        this.color = color;
    }
    public int getAlienId() {
        return alienId;
    }
    public void setAlienId(int alienId) {
        this.alienId = alienId;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }
    public String getColor() {
        return color;
    }
    public void setColor(String color) {
        this.color = color;
    }


}
