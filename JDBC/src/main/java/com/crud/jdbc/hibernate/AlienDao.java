package com.crud.jdbc.hibernate;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class AlienDao {
    public void createAlien(Alien alien){
        Transaction transaction = null;
        try {
            Session session = Util.getSessionFactory().openSession();
            // start a transaction
            transaction = session.beginTransaction();
            // save the person object
            session.save(alien); // INSERT
            // commit transaction
            transaction.commit();
        } catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
        }
    }
    public void updateAlien(Alien alien, int id){
        Transaction transaction = null;
        try {
            Session session = Util.getSessionFactory().openSession();
            // start a transaction
            transaction = session.beginTransaction();
            // save the person object
            alien.setAlienId(id);
            session.update(alien);
            // commit transaction
            transaction.commit();
        } catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
        }
    }
    public void deleteAlien(Alien alien, int id){
        Transaction transaction = null;
        try {
            Session session = Util.getSessionFactory().openSession();
            // start a transaction
            transaction = session.beginTransaction();
            // save the person object
            alien.setAlienId(id);
            session.delete(alien);
            // commit transaction
            transaction.commit();
        } catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
        }
    }
    public void findAlien(int id){
        Transaction transaction = null;
        try {
            Session session = Util.getSessionFactory().openSession();
            // start a transaction
            transaction = session.beginTransaction();
            Alien alien = session.find(Alien.class, id);
            System.out.println(alien.getColor() +" "+ alien.getName());
            // commit transaction
            transaction.commit();
        } catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
        }
    }
    public void deleteById(int id){
        Transaction transaction = null;
        try {
            Session session = Util.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            Alien alien = null;
            alien = session.load(Alien.class, id);

            session.delete(alien);
            transaction.commit();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
    }
}
