package com.crud.jdbc.hibernate;

public class App {
    public static void main(String args[]){
        Alien alien1 = new Alien();
        alien1.setAlienId(1);
        alien1.setColor("green");
        alien1.setName("John");
        alien1.setType("galactic");
        Alien alien2 = new Alien("Gigi", "Planetary", "Red");
        Alien alien3 = new Alien("Victor", "Interstellar", "Tan");
        AlienDao objHelper = new AlienDao();
//        objHelper.createAlien(alien1);
//        objHelper.updateAlien(alien3,4);
//        objHelper.createAlien(alien2);
        objHelper.findAlien(2);
    }
}

//    Transaction transaction = null;
//
//        try{
//                Session session = Util.getSessionFactory().openSession();
//
//                transaction = session.beginTransaction();
//
//                session.save(alien1);
//
//                transaction.commit();
//                }catch (Exception e){
//                e.printStackTrace();
//                }

//Pt a folosi fisierul xml din resources:
/////.addAnnotatedClass(Alien.class) -> asta inseamna ca mentionam in main ca clasa Alien se mapeaza pe tabela alien, sau mentionam in bd
//        Configuration con = new Configuration().configure();
//        SessionFactory sf = con.buildSessionFactory();
//        Session session = sf.openSession();
//
//        Transaction tx = session.beginTransaction();
//        session.save(alien2);
//        tx.commit();
//
