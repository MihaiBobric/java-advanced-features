package com.crud.jdbc.hibernateExercise;

import org.hibernate.Session;
import org.hibernate.Transaction;

import static com.crud.jdbc.hibernateExercise.Util.getSessionFactory;

public class EmployeeDao {
    public void createEmployee(EmployeeEnt employee){
        Transaction transaction = null;
        try {
            Session session = Util.getSessionFactory().openSession();
            // start a transaction
            transaction = session.beginTransaction();
            // save the person object
            session.save(employee); // INSERT
            // commit transaction
            transaction.commit();
        } catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
        }
    }
    public void updateEmployee(EmployeeEnt employee, int id){
        Transaction transaction = null;
        try {
            Session session = getSessionFactory().openSession();
            // start a transaction
            transaction = session.beginTransaction();
            // save the person object
            employee.setEmployeeId(id);
            session.update(employee);
            // commit transaction
            transaction.commit();
        } catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
        }
    }
    public void deleteEmployee(EmployeeEnt employee, int id){
        Transaction transaction = null;
        try {
            Session session = getSessionFactory().openSession();
            // start a transaction
            transaction = session.beginTransaction();
            // save the person object
            employee.setEmployeeId(id);
            session.delete(employee);
            // commit transaction
            transaction.commit();
        } catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
        }
    }
    public void findEmployee(int id){
        Transaction transaction = null;
        try {
            Session session = getSessionFactory().openSession();
            // start a transaction
            transaction = session.beginTransaction();
            EmployeeEnt employee = session.find(EmployeeEnt.class, id);
            System.out.println(employee.getFirstName() + " "+employee.getLastName());
            // commit transaction
            transaction.commit();
        } catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
        }
    }
}
