package com.crud.jdbc.hibernateExercise;

public class App {
    public static void main(String[] args) {
        EmployeeEnt employee = new EmployeeEnt(5,"Alex","Escu","2020-01-01","555-4545");
        EmployeeEnt employee2 = new EmployeeEnt(6,"Alex","Sonea","2020-01-01","555-4545");
        EmployeeDao employeeHelper=new EmployeeDao();
//        employeeHelper.createEmployee(employee2);
        employeeHelper.findEmployee(2);
        employeeHelper.updateEmployee(employee,8);
    }
}
