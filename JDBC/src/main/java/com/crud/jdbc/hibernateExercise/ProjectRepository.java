package com.crud.jdbc.hibernateExercise;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class ProjectRepository {
    public void createProject(ProjectEnt project){
        Transaction transaction = null;

        try {
            Session session = Util.getSessionFactory().openSession();
            // start a transaction
            transaction = session.beginTransaction();
            // save the person object
            session.save(project);
            // commit transaction
            transaction.commit();
        }

        catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
        }
    }

    public void findById(int projectId) {
        Transaction transaction = null;
        try {
            Session session = Util.getSessionFactory().openSession();
            // start a transaction
            transaction = session.beginTransaction();
            ProjectEnt project = session.find(ProjectEnt.class, projectId);
            System.out.println(project.getProjectId());
            // commit transaction
            transaction.commit();
        } catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
        }
    }

    public void updateProject(ProjectEnt project, int projectId) {
        Transaction transaction = null;

        try {
            Session session = Util.getSessionFactory().openSession();
            // start a transaction
            transaction = session.beginTransaction();
            // save the person object
            project.setProjectId(projectId);
            session.update(project);
            // commit transaction
            transaction.commit();
        }
        catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
        }
    }
}