package com.crud.jdbc.hibernateExercise;

import javax.persistence.*;

@Entity
@Table(name="projects")
public class ProjectEnt {
@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
@Column(name="projectId")
    private int projectId;
    @Column(name="description")
    private String description;
    public ProjectEnt() {
    }
    public ProjectEnt(int projectId, String description) {
        this.projectId = projectId;
        this.description = description;
    }

    public int getProjectId() {
        return projectId;
    }

    public void setProjectId(int projectId) {
        this.projectId = projectId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "ProjectEnt{" +
                "projectId=" + projectId +
                ", description='" + description + '\'' +
                '}';
    }
}
