package com.sda.practical.views;

import com.sda.practical.services.AuthorServices;
import com.sda.practical.services.BookServices;
import com.sda.practical.services.HibernateService;
import com.sda.practical.services.ReviewServices;

import java.util.Scanner;

public class ConsoleViewHandler {
    private MenuHandler menuHandler;
    private AuthorServices authorServices;
    private BookServices bookServices;
    private HibernateService hibernateService;
    private ReviewServices reviewService;

    public ConsoleViewHandler() {
        this.menuHandler = new MenuHandler();
        this.hibernateService = new HibernateService();
        this.authorServices = new AuthorServices(hibernateService);
        this.bookServices = new BookServices(hibernateService);
        this.reviewService=new ReviewServices(hibernateService);

        System.out.println(getClass().getSimpleName() + " created");
    }

    public void start() {
        Scanner scanner = new Scanner(System.in);

        Integer option = 0;
        while (option != 9) {
            menuHandler.printMenu(MenuTypeEnum.MAIN_MENU);
            option = scanner.nextInt();

            switch (option) {
                case 1:
                    authorFlow(scanner);
                    break;
                case 2:
                    bookFlow(scanner);
                    break;
                case 3:
                    reviewFlow(scanner);
                    break;
                case 9:
                    break;
                default:
                    System.out.println("Selectati o optiune");
            }
        }

    }

    public void authorFlow(Scanner scanner) {
        Integer option = 0;
        while (option != 9) {
            menuHandler.printMenu(MenuTypeEnum.AUTHOR_MENU);
            option = scanner.nextInt();
            scanner.nextLine();
            switch (option) {
                case 1:
                    this.authorServices.viewAuthors();
                    break;
                case 2:
                    this.authorServices.addAuthor(scanner);
                    break;
                case 3:
                    this.authorServices.editAuthor(scanner);
                    break;
                case 4:
                    this.authorServices.deleteAuthor(scanner);
                    break;
                default:
                    System.out.println("Selectati o optiune");
            }
        }

    }

    public void bookFlow(Scanner scanner) {
        Integer option = 0;
        while (option != 9) {
            menuHandler.printMenu(MenuTypeEnum.BOOK_MENU);
            option = scanner.nextInt();
            scanner.nextLine();
            switch (option) {
                case 1:
                    this.bookServices.viewBooks();
                    break;
                case 2:
                    this.bookServices.addBook(scanner);
                    break;
                case 3:
                    this.bookServices.editBook(scanner);
                    break;
                case 4:
                    this.bookServices.deleteBook(scanner);
                    break;
                default:
                    System.out.println("Selectati o optiune");
            }
        }
    }

    public void reviewFlow(Scanner scanner) {
        Integer option = 0;
        while (option != 9) {
            menuHandler.printMenu(MenuTypeEnum.REVIEW_MENU);
            option = scanner.nextInt();
            scanner.nextLine();
            switch (option) {
                case 1:
                    this.reviewService.viewReview();
                    break;
                case 2:
                    this.reviewService.addReview(scanner);
                    break;
                default:
                    System.out.println("Selectati o optiune");
            }
        }
    }
}
