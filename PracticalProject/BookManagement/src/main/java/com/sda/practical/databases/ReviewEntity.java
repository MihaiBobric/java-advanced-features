package com.sda.practical.databases;

import javax.persistence.*;

@Entity
@Table(name="reviews")
public class ReviewEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer reviewId;
    private String title;
    private String review;

//    @Column(insertable=false,updatable=false)
    private Integer bookId; //daca vrem sa updatam manual id-ul
    //daca nu il updatam manual, chemam metoda din BaseServices

    @ManyToOne
    @JoinColumn(name="bookId", insertable=false,updatable=false)
    private BookEntity book;

    public Integer getReviewId() {
        return reviewId;
    }

    public void setReviewId(Integer reviewId) {
        this.reviewId = reviewId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getComment() {
        return review;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String comment) {
        this.review = comment;
    }

    public BookEntity getBook() {
        return book;
    }

    public void setBook(BookEntity book) {
        this.book = book;
    }

    public Integer getBookId() {
        return bookId;
    }

    public void setBookId(Integer bookId) {
        this.bookId = bookId;
    }

    @Override
    public String toString() {
        return "ReviewEntity{" +
                "reviewId=" + reviewId +
                ", title='" + title + '\'' +
                ", comment='" + review + '\'' +
                ", book=" + book +
                '}';
    }
}
