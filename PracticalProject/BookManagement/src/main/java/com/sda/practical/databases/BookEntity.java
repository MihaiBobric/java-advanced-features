package com.sda.practical.databases;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="books")
public class BookEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer bookId;
    private String name;
    private String isbn;
    private String publishingHouse;
    private Integer year;
    private String numberOfReviews;

//    @Column(insertable=false,updatable=false)
    private Integer authorId; //daca vrem sa updatam manual id-ul
    //daca nu il updatam manual, chemam metoda din BaseServices

    @ManyToOne
    @JoinColumn(name="authorId",insertable=false,updatable=false)
    private AuthorEntity author;

    @OneToMany(mappedBy = "book", fetch=FetchType.EAGER) //ia de la inceput lista de review-uri in memorie ,
    // ca sa nu trebuiasca sa o citim de fiecare data cand apelam gatterul getBooks
    private List<ReviewEntity> reviews;

    public Integer getBookId() {
        return bookId;
    }

    public void setBookId(Integer bookId) {
        this.bookId = bookId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getPublishingHouse() {
        return publishingHouse;
    }

    public void setPublishingHouse(String publishingHouse) {
        this.publishingHouse = publishingHouse;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public AuthorEntity getAuthor() {
        return author;
    }

    public void setAuthor(AuthorEntity author) {
        this.author = author;
    }

    public Integer getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Integer authorId) {
        this.authorId = authorId;
    }

    public List<ReviewEntity> getReviews() {
        return reviews;
    }

    public void setReviews(List<ReviewEntity> reviews) {
        this.reviews = reviews;
    }

    public String getNumberOfReviews() {
        return numberOfReviews;
    }

    public void setNumberOfReviews(String numberOfReviews) {
        this.numberOfReviews = numberOfReviews;
    }

}
