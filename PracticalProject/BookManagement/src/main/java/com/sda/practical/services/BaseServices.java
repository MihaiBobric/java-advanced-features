package com.sda.practical.services;

import com.sda.practical.databases.AuthorEntity;
import com.sda.practical.databases.BookEntity;
import org.hibernate.Session;
import org.hibernate.Transaction;

public abstract class BaseServices {
private HibernateService hibernateService;

    public BaseServices(HibernateService hibernateService) {
        this.hibernateService = hibernateService;
    }

    public HibernateService getHibernateService() {
        return hibernateService;
    }

    protected AuthorEntity getAuthor(Integer authorId) {
        Session session = getHibernateService().getSessionFactory().openSession();
        AuthorEntity authorEntity = session.find(AuthorEntity.class, authorId);
        session.close();
        return authorEntity;
    }
    protected void deleteBook(BookEntity bookEntity) {
        Session session = getHibernateService().getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.delete(bookEntity);
        transaction.commit();
        session.close();
    }
}
