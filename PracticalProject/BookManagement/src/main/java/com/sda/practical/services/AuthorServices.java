package com.sda.practical.services;

import com.sda.practical.databases.AuthorEntity;
import com.sda.practical.databases.BookEntity;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;
import java.util.Scanner;

public class AuthorServices extends BaseServices {

    public AuthorServices(HibernateService hibernateService) {
        super(hibernateService);
        System.out.println(getClass().getSimpleName() + " created");
    }

    public void viewAuthors() {
        List<AuthorEntity> authors = getAllAuthors();
        authors.stream().forEach(b -> {
            System.out.printf("%15s",b.getName());
        });
    }

    public void editAuthor(Scanner scanner) {
//       1 afiseaza lista de autori
        List<AuthorEntity> authors = getAllAuthors();
        authors.stream().forEach(a -> System.out.println(a.getAuthorId() + ". " + a.getName()));
//       2 cere sa selecteze autor
        System.out.println("selectati un autor");
        Integer authorId = scanner.nextInt();
        scanner.nextLine();
//       3 editeaza
        AuthorEntity authorEntity = getAuthor(authorId);
        System.out.println("Introduceti numele autorului");
        String name = scanner.nextLine();
//       4 salveaza
        authorEntity.setName(name);
        updateAuthor(authorEntity);
    }

    public void addAuthor(Scanner scanner) {
//       1 creaza o entitate
        AuthorEntity authorEntity=new AuthorEntity();
//       2 cere sa introduca randul
        System.out.println("Introduceti numele");
        String name= scanner.nextLine();
//       3 salveaza
        authorEntity.setName(name);
        saveAuthor(authorEntity);

    }
    public void deleteAuthor(Scanner scanner){
        List<AuthorEntity> authors = getAllAuthors();
        authors.stream().forEach(a -> System.out.println(a.getAuthorId() + ". " + a.getName()+ " "+a.getBooks()));
        System.out.println("selectati un autor");
        Integer authorId = scanner.nextInt();
        scanner.nextLine();
        AuthorEntity authorEntity=getAuthor(authorId);
        if (authorEntity.getBooks()!=null){
            System.out.println("Stergeti si cartile? Y/N");
            String option= scanner.nextLine();
            if (option.toLowerCase().equals("y")){
                for(BookEntity bookEntity:authorEntity.getBooks()){
                    deleteBook(bookEntity);
                }
            }else if (option.toLowerCase().equals("n")){
                System.out.println("Autorul nu a fost sters");
                return;
            }else {System.out.println("Selectati o optiune valida");
            return;}
        }
        deleteAuthor(authorEntity);
    }

    private List<AuthorEntity> getAllAuthors() {
        Session session = getHibernateService().getSessionFactory().openSession();
        return session.createQuery("from AuthorEntity", AuthorEntity.class).list();

    }



    private void updateAuthor(AuthorEntity authorEntity) {
        Session session = getHibernateService().getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.update(authorEntity);
        transaction.commit();
        session.close();
    }
    private void saveAuthor(AuthorEntity authorEntity) {
        Session session = getHibernateService().getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.save(authorEntity);
        transaction.commit();
        session.close();
    }
    private void deleteAuthor(AuthorEntity authorEntity){
//  rescriem metoda
        Session session = getHibernateService().getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.delete(authorEntity);
        transaction.commit();
        session.close();
    }
}
