package com.sda.practical.services;

import com.sda.practical.databases.BookEntity;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;
import java.util.Scanner;

public class BookServices extends BaseServices {
    public BookServices(HibernateService hibernateService) {
        super(hibernateService);
        System.out.println(getClass().getSimpleName() + " created");
    }

    public void viewBooks() {
        List<BookEntity> books = getAllBooks();
        books.stream().forEach(b -> {
            System.out.printf("%d - %15s - %15s - %15s - %d - %15s - %d", b.getBookId(), b.getIsbn(), b.getName()
                    , b.getPublishingHouse(), b.getYear(), b.getAuthor() != null ? b.getAuthor().getName() : "", b.getReviews() != null ? b.getReviews().size() : 0);
            System.out.println();
        });

    }

    public void editBook(Scanner scanner) {
        List<BookEntity> books = getAllBooks();
        books.stream().forEach(a -> System.out.println(a.getBookId() + ". isbn:" + a.getIsbn() + ". " + a.getName() + ". " +
                a.getPublishingHouse() + ". " + a.getYear() + ". Id-ul autorului:" + a.getAuthor()));
        System.out.println("selectati o carte");
        Integer bookId = scanner.nextInt();
        scanner.nextLine();
        BookEntity bookEntity = getBook(bookId);
        System.out.println("Introduceti isbn");
        String isbn = scanner.nextLine();
        bookEntity.setIsbn(isbn);
        System.out.println("Introduceti numele cartii");
        String name = scanner.nextLine();
        bookEntity.setName(name);
        System.out.println("Introduceti editura");
        String pH = scanner.nextLine();
        bookEntity.setPublishingHouse(pH);
        System.out.println("Introduceti anul editiei");
        Integer year = scanner.nextInt();
        bookEntity.setYear(year);
        System.out.println("Introduceti Id-ul autorului");
        Integer id = scanner.nextInt();
//        AuthorEntity author = getAuthor(id); //facem asta cu metoda din BaseServices
//        bookEntity.setAuthor(author);
        bookEntity.setAuthorId(id);
        updateBook(bookEntity);
    }

    public void addBook(Scanner scanner) {
        BookEntity bookEntity = new BookEntity();
        System.out.println("Introduceti isbn");
        String isbn = scanner.nextLine();
        bookEntity.setIsbn(isbn);
        System.out.println("Introduceti numele cartii");
        String name = scanner.nextLine();
        bookEntity.setName(name);
        System.out.println("Introduceti editura");
        String pH = scanner.nextLine();
        bookEntity.setPublishingHouse(pH);
        System.out.println("Introduceti anul editiei");
        Integer year = scanner.nextInt();
        bookEntity.setYear(year);
        System.out.println("Introduceti Id-ul autorului");
        Integer id = scanner.nextInt();
        //        AuthorEntity author = getAuthor(id); //facem asta cu metoda din BaseServices
        //        bookEntity.setAuthor(author);
        bookEntity.setAuthorId(id);
        saveBook(bookEntity);

    }

    public void deleteBook(Scanner scanner) {
        List<BookEntity> books = getAllBooks();
        books.stream().forEach(a -> System.out.println(a.getBookId() + ". isbn:" + a.getIsbn() + ". " + a.getName() + ". " +
                a.getPublishingHouse() + ". " + a.getYear() + ". Id-ul autorului:" + a.getAuthor()));
        System.out.println("selectati o carte");
        Integer bookId = scanner.nextInt();
        scanner.nextLine();
        BookEntity bookEntity = getBook(bookId);
        deleteBook(bookEntity);
    }

    private List<BookEntity> getAllBooks() {
        Session session = getHibernateService().getSessionFactory().openSession();
        return session.createQuery("from BookEntity", BookEntity.class).list();

    }


    private BookEntity getBook(Integer bookId) {
        Session session = getHibernateService().getSessionFactory().openSession();
        BookEntity bookEntity = session.find(BookEntity.class, bookId);
        session.close();
        return bookEntity;
    }

    private void updateBook(BookEntity bookEntity) {
        Session session = getHibernateService().getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.update(bookEntity);
        transaction.commit();
        session.close();
    }

    private void saveBook(BookEntity bookEntity) {
        Session session = getHibernateService().getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.save(bookEntity);
        transaction.commit();
        session.close();
    }




}
