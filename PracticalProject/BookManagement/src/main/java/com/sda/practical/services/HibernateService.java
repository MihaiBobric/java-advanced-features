package com.sda.practical.services;

import com.sda.practical.databases.AuthorEntity;
import com.sda.practical.databases.BookEntity;
import com.sda.practical.databases.ReviewEntity;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;
import org.hibernate.service.ServiceRegistry;

import java.util.Properties;

public class HibernateService {
//    private static HibernateService hibernateService;
//
//    public static HibernateService getSession() {
//        if (hibernateService == null) {
//            hibernateService = new HibernateService();
//        }
//        return hibernateService;

//    } // singleton

    private SessionFactory sessionFactory;

    public HibernateService() {
        System.out.println(getClass().getSimpleName() + " created");
    }

    public SessionFactory getSessionFactory() {
        if (sessionFactory == null) {
            try {
                Configuration configuration = new Configuration();
// Hibernate settings equivalent to hibernate.cfg.xml's properties
                Properties settings = new Properties();
                settings.put(Environment.DRIVER, "com.mysql.jdbc.Driver");
                settings.put(Environment.URL, "jdbc:mysql://localhost:3306/practicalproject?serverTimezone=UTC");
                settings.put(Environment.USER, "root");
                settings.put(Environment.PASS, "Ralulove");
                settings.put(Environment.DIALECT, "org.hibernate.dialect.MySQL5Dialect");
                settings.put(Environment.SHOW_SQL, "true");
                settings.put(Environment.CURRENT_SESSION_CONTEXT_CLASS, "thread");
                settings.put(Environment.HBM2DDL_AUTO, "update");
                configuration.setProperties(settings);
                configuration.addAnnotatedClass(AuthorEntity.class);
                configuration.addAnnotatedClass(BookEntity.class);
                configuration.addAnnotatedClass(ReviewEntity.class);
                ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();

                sessionFactory = configuration.buildSessionFactory(serviceRegistry);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
        return sessionFactory;
    }
}
