package com.sda.practical.views;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MenuHandler {
    private Map<MenuTypeEnum, List<String>> menus;
    public MenuHandler() {
        menus = new HashMap<>();
        List<String> mainMenu = new ArrayList<>();
        mainMenu.add("1. Management Autori");
        mainMenu.add("2. Management Carti");
        mainMenu.add("3. Management Reviews");
        mainMenu.add("9. Exit");
        menus.put(MenuTypeEnum.MAIN_MENU, mainMenu);

        List<String> authorMenu = new ArrayList<>();
        authorMenu.add("1.Vizualizeaza lista de autori");
        authorMenu.add("2.Adauga un autor");
        authorMenu.add("3.Actualizeaza un autor");
        authorMenu.add("4.Sterge un autor");
        authorMenu.add("9.Intoarcere la meniul principal");
        menus.put(MenuTypeEnum.AUTHOR_MENU, authorMenu);

        List<String> booksMenu = new ArrayList<>();
        booksMenu.add("1.Vizualizeaza lista de carti");
        booksMenu.add("2.Adauga o carte");
        booksMenu.add("3.Actualizeaza o carte");
        booksMenu.add("4.Sterge o carte");
        booksMenu.add("9.Intoarcere la meniul principal");
        menus.put(MenuTypeEnum.BOOK_MENU, booksMenu);

        List<String> reviewMenu = new ArrayList<>();
        reviewMenu.add("1.Vizualizeaza review-uri");
        reviewMenu.add("2.Adauga un review");
        reviewMenu.add("9.Intoarcere la meniul principal");
        menus.put(MenuTypeEnum.REVIEW_MENU, reviewMenu);
    }
    public void printMenu(MenuTypeEnum menuType) {

//        List<String> menuList = menus.get(menuType);
//        for (String menuValue : menuList) {
//            System.out.println(menuValue);
//        }
        menus.getOrDefault(menuType,new ArrayList<>()).stream().forEach(System.out::println);
    }
}
