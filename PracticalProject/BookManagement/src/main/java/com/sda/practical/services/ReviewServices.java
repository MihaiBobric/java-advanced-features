package com.sda.practical.services;

import com.sda.practical.databases.ReviewEntity;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;
import java.util.Scanner;

public class ReviewServices extends BaseServices{
    public ReviewServices(HibernateService hibernateService) {
        super(hibernateService);
        System.out.println(getClass().getSimpleName()+" created");
    }
public void viewReview(){
    List<ReviewEntity> reviewEntities=getAllReviews();
    reviewEntities.stream().forEach(reviewEntity -> {
        System.out.printf("%15s",reviewEntity.getReview());
    });
    System.out.println();
}
    private List<ReviewEntity> getAllReviews(){
        Session session = getHibernateService().getSessionFactory().openSession();
        return session.createQuery("from ReviewEntity", ReviewEntity.class).list();
    }
    public void addReview(Scanner scanner) {
        ReviewEntity reviewEntity=new ReviewEntity();
        System.out.println("Introduceti titlul");
        String title= scanner.nextLine();
        reviewEntity.setTitle(title);
        System.out.println("Introduceti recenzia");
        String review= scanner.nextLine();
        reviewEntity.setReview(review);
        System.out.println("Introduceti id-ul cartii");
        Integer id=scanner.nextInt();
        reviewEntity.setBookId(id);
        saveReview(reviewEntity);

    }
    private void saveReview(ReviewEntity reviewEntity) {
        Session session = getHibernateService().getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.save(reviewEntity);
        transaction.commit();
        session.close();
    }
}
