package Inheritance;

public class Scissors extends Tool{

    private String colour;

    public Scissors(){
        super();
        this.colour="";
    }

    public Scissors(String name, String scope, String price, String colour) {
        super(name, scope, price);
        this.colour = colour;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    @Override
    public String toString() {
        return "Scissors{" +
                "colour='" + colour + '\'' +
                ", name='" + name + '\'' +
                ", scope='" + scope + '\'' +
                ", price='" + price + '\'' +
                '}';
    }
}
