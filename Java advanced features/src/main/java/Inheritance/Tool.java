package Inheritance;

public class Tool {

    protected String name;
    protected String scope;
    protected String price;

    public Tool(){
        this.name="";
        this.scope="";
        this.price="";
    }

    public Tool(String name, String scope, String price) {
        this.name = name;
        this.scope = scope;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Tool{" +
                "name='" + name + '\'' +
                ", scope='" + scope + '\'' +
                ", price='" + price + '\'' +
                '}';
    }
}
