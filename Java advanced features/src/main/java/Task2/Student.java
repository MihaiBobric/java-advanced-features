package Task2;

public class Student extends Person {
    private String typeOfStudy;
    private int yearOfStudy;
    private int studyPrice;

    public Student(String name, String address, String typeOfStudy, int yearOfStudy, int studyPrice) {
        super(name, address);
        this.typeOfStudy = typeOfStudy;
        this.yearOfStudy = yearOfStudy;
        this.studyPrice = studyPrice;
    }

    public Student(String typeOfStudy, int yearOfStudy, int studyPrice) {
        super();
        this.typeOfStudy = typeOfStudy;
        this.yearOfStudy = yearOfStudy;
        this.studyPrice = studyPrice;
    }

    public String getTypeOfStudy() {
        return typeOfStudy;
    }

    public void setTypeOfStudy(String typeOfStudy) {
        this.typeOfStudy = typeOfStudy;
    }

    public int getYearOfStudy() {
        return yearOfStudy;
    }

    public void setYearOfStudy(int yearOfStudy) {
        this.yearOfStudy = yearOfStudy;
    }

    public int getStudyPrice() {
        return studyPrice;
    }

    public void setStudyPrice(int studyPrice) {
        this.studyPrice = studyPrice;
    }

public String string(){
        return super.toString();
}

    @Override
    public String toString() {
        return "Student{" +
                "typeOfStudy='" + typeOfStudy + '\'' +
                ", yearOfStudy=" + yearOfStudy +
                ", studyPrice=" + studyPrice +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                '}';
    }
@Override
    public void f (){
        System.out.println("Price of college = " + (yearOfStudy*studyPrice));
    }
}
