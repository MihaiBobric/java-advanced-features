package Task2;

public class Main {
    public static void main(String[] args) {
        Student mihai = new Student("Programming", 2,25);
        Staff profesor = new Staff("AdvancedJava", 9999);

        System.out.println(mihai.toString());
        System.out.println(profesor.toString());
        mihai.setName("mihai");
        mihai.setAddress("Bucuresti");
        Exercitiu(mihai);
        Person student= new Student("Student",2,4500);
        System.out.println(student);

        String s = mihai.string();
        System.out.println(s);
        mihai.f();
        profesor.f();

    }

    public static void Exercitiu(Person person){
        System.out.println(person.getName()+".");
        System.out.println(person.getAddress()+".");
    }
}
