package advanced_features_coding.day1;

/*
Testați sistemul astfel creat implementând un program care îi cere utilizatorului
numele și afișează un mesaj de bun venit și numărul de litere din numele introdus;
 */
public class Exercitiu3 implements Program {
    /*Afisam utilizatorului inainte sa-i cerem input*/
    public String getDescription() {
        return "What's your name?";
    }
    /*Procesam input de la utilizator si afisam rezultatul*/
    public void run(String userInput) {
        System.out.println("Hello " + userInput); /*mesajul de bun venit*/
        /*numarul de litere*/
        System.out.println("Numele tau are " + userInput.length() + " litere");
    }
    public String toString() {return "Ziua 1 - exercitiul 3";}
}
