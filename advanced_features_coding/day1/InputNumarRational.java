package advanced_features_coding.day1;
/*
Implementeaza interactiunea cu utilizatorul pentru programe care necesita un
numar natural ca input (ex: 5)
Afiseaza utilizatorului o eroare daca input-ul introdus nu este numar natural.
 */
public abstract class InputNumarRational implements Program {
    /*Descriere pentru utilizator in cazul normal de functionare*/
    public static final String NORMAL_DESCRIPTION = "Introdu un numar";
    /*Descriere pentru utilizator in cazul unui input gresit*/
    public static final String BAD_NUMBER_DESCRIPTION = " nu este un numar";

    /*incepem cu functionarea normala*/
    protected String description = InputNumarRational.NORMAL_DESCRIPTION;
    /*Afisam utilizatorului inainte sa-i cerem input*/
    public String getDescription() {
        return description;
    }

    /*Procesam input de la utilizator si afisam rezultatul*/
    public void run(String userInput) {
        double n; /*pentru numarul introdus de utilizator*/
        try {
            n = Double.parseDouble(userInput); /*incercam sa extragem numarul
                                            din variabila de tip String*/
            /*daca operatia anterioara a esuat*/
            /*codul "sare" la clauza catch*/
            /*deci daca ajungem la linia urmatoare*/
            /*stim ca utilizatorul a introdus un numar rational*/
            /*si actualizam descrierea in cazul in care anterior era eroare*/
            description = InputNumarRational.NORMAL_DESCRIPTION;
        } catch (Exception e) {
            /*daca am ajuns aici, utilizatorul a introdus altceva decat un*/
            /*numar rational; actualizam descrierea cu eroarea potrivita*/
            description = userInput + InputNumarRational.BAD_NUMBER_DESCRIPTION;
            return; /*terminam executia functiei deoarece am intalnit o eroare*/
        }
        /*daca am ajuns aici avem input corect de la utilizator
        si putem executa programul mostenitor*/
        run(n);
    }

    /*programele cu input un numar natural pot mosteni din aceasta clasa si vor
    primi numarul introdus de utilizator ca parametru la functia run(double)*/
    public abstract void run(double userInput);
}
