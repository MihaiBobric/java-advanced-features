package advanced_features_coding.day1;

/*
Folosind sistemul creat la punctele 1-3 creați un program care îi cere
utilizatorului un număr natural N și apoi afișează numărul introdus citit
de la dreapta la stânga (inversarea ordinii cifrelor);
 */
public class Exercitiu6 extends InputNumarNatural {
    /*Procesam input de la utilizator si afisam rezultatul*/
    /*deoarece mostenim din InputNumarNatural primim direct input-ul sub
    forma unei variabile de tip int*/
    public void run(int n) {
        int rez = 0; /*aici vom salva numarul inversat*/
        while(n != 0) { /*cat timp mai avem cifre*/
            rez = rez * 10 + n % 10; /*adaugam ultima cifra la rezultat*/
            n = n / 10; /*stergem ultima cifra a numarului si repetam*/
        }
        System.out.println(rez); /*afisam numarul inversat*/
    }
    public String toString() {return "Ziua 1 - exercitiul 6";}
}
