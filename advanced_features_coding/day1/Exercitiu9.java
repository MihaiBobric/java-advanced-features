package advanced_features_coding.day1;
/*
Folosind sistemul creat la punctele 1-3 creați un program care îi cere
utilizatorului un număr natural N, apoi N numere raționale și afișează
media aritmetică a acestora;
 */
public class Exercitiu9 extends InputDoubleArray {
    /*Procesam input de la utilizator si afisam rezultatul*/
    /*deoarece mostenim din InputDoubleArray primim direct input-ul sub
    forma membrului list*/
    public void run() {
        double sum = 0.0; //suma numerelor citite
        for(int i = 0; i < list.size(); ++i) { //parcurg lista cu numere
            sum += list.get(i); //adaug la suma
        }
        System.out.println(sum / list.size()); //afisez media aritmetica
        description = InputDoubleArray.INPUT_N; //resetez descrierea pentru executia urmatoare
        state = InputDoubleArray.GETTING_N; //resetez starea pentru executia urmatoare
    }
    public String toString() {return "Ziua 1 - exercitiul 9";}
}
