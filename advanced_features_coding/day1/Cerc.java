package advanced_features_coding.day1;
/*
Implementarea unui cerc pentru Exercitiu12
 */
public class Cerc implements Forma {
    /*Descriere pentru utilizator in cazul normal de functionare*/
    public static final String NORMAL_DESCRIPTION = "Introdu lungimea razei";
    /*Descriere pentru utilizator in cazul unui input gresit*/
    public static final String BAD_INPUT = " nu e o lungime";
    /*incepem cu functionarea normala*/
    private String description = Cerc.NORMAL_DESCRIPTION;
    /*raza cercului*/
    private double R;
    /*daca am terminat de luat input de la utilizator*/
    private boolean finish = false;
    /*Afisam utilizatorului inainte sa-i cerem input*/
    public String getDescription() {return description;}
    public boolean finished(){
        if(finish) { /*daca am terminat de luat input de la utilizator*/
            finish = false; /*resetam instanta curenta pentru o noua executie*/
            return true; /*si returnam faptul ca am terminat de luat input de la utilizator*/
        }
        return finish; /*altfel: returnam daca am terminat de luat input de la utilizator*/
    }
    /*Procesam input de la utilizator si il salvam in variabila membru*/
    public void run(String userInput){
        try {
            R = Double.parseDouble(userInput); /*incercam sa extragem raza din String-ul primit*/
            /*daca operatia anterioara a esuat*/
            /*codul "sare" la clauza catch*/
            /*deci daca ajungem la linia urmatoare*/
            /*stim ca utilizatorul a introdus raza corect*/
            /*si actualizam descrierea in cazul in care anterior era eroare*/
            description = Cerc.NORMAL_DESCRIPTION;
        } catch (Exception e) {
            /*daca am ajuns aici, utilizatorul a introdus raza gresit
            actualizam descrierea cu eroarea potrivita*/
            description = userInput + Cerc.BAD_INPUT;
            return;
        }
        finish = true; /*semnalam faptul ca am terminat de luat input de la utilizator*/
    }
    /*returnam circumferinta cercului*/
    public double perimetru() {
        return 2 * Math.PI * R;
    }
    /*returnam suprafata cercului*/
    public double aria() {
        return Math.PI * R * R;
    }
}