package advanced_features_coding.day1;
/*
Implementarea unui dreptunghi pentru Exercitiu12
 */
public class Dreptunghi implements Forma {
    /*Descriere pentru utilizator in cazul normal de functionare*/
    public static final String NORMAL_DESCRIPTION = "Introdu dimensiunile laturilor " +
            "pe aceeasi linie";
    /*Descriere pentru utilizator in cazul unui input gresit*/
    public static final String BAD_INPUT = "Nu inteleg ";
    /*incepem cu functionarea normala*/
    private String description = Dreptunghi.NORMAL_DESCRIPTION;
    /*lungimea dreptunghiului*/
    private double L;
    /*latimea dreptunghiului*/
    private double l;
    /*daca am terminat de luat input de la utilizator*/
    private boolean finish = false;
    /*Afisam utilizatorului inainte sa-i cerem input*/
    public String getDescription() {return description;}
    public boolean finished(){
        if(finish) { /*daca am terminat de luat input de la utilizator*/
            finish = false; /*resetam instanta curenta pentru o noua executie*/
            return true; /*si returnam faptul ca am terminat de luat input de la utilizator*/
        }
        return finish; /*altfel: returnam daca am terminat de luat input de la utilizator*/
    }
    /*Procesam input de la utilizator si il salvam in variabila membru*/
    public void run(String userInput){
        try {
            /*cautam doua numere separate prin spatiu*/
            String[] cuvinte = userInput.split(" "); /*separam cele doua componente*/
            /*incercam sa extragem lungimea din prima componenta*/
            L = Double.parseDouble(cuvinte[0]);
            /*incercam sa extragem latimea din ultima componenta*/
            /*in caz ca utilizatorul a introdus mai multe spatii*/
            l = Double.parseDouble(cuvinte[cuvinte.length - 1]);
            /*daca vreuna din operatiile anterioare a esuat*/
            /*codul "sare" la clauza catch*/
            /*deci daca ajungem la linia urmatoare*/
            /*stim ca utilizatorul a introdus datele corect*/
            /*si actualizam descrierea in cazul in care anterior era eroare*/
            description = Dreptunghi.NORMAL_DESCRIPTION;
        } catch (Exception e) {
            /*daca am ajuns aici, utilizatorul a introdus datele gresit
            actualizam descrierea cu eroarea potrivita*/
            description = Dreptunghi.BAD_INPUT + userInput;
            return;
        }
        finish = true;
    }
    /*returnam suma lungimilor laturilor*/
    public double perimetru() {
        return 2 * (l + L);
    }
    /*returnam suprafata dreptunghiului*/
    public double aria() {
        return l * L;
    }
}
