package advanced_features_coding.day1;

import java.util.ArrayList;

/*
Implementeaza interactiunea cu utilizatorul pentru programe care necesita un
array de numere rationale (cu virgula) ca input (ex: [5.3, 2.7])
Afiseaza utilizatorului o eroare daca input-ul introdus nu este corect.
 */
public abstract class InputDoubleArray implements Program {
    /*Descriere pentru utilizator inainte sa citim N de la tastatura*/
    public static final String INPUT_N = "How many numbers do you have?";
    /*Descriere pentru utilizator in timp ce citim cele N numere de la tastatura*/
    public static final String INPUT_NUMBERS = "Enter the next number";
    /*Descriere pentru utilizator pentru eroarea: N introdus gresit*/
    public static final String BAD_N = " does not reflect how many numbers you have";
    /*Descriere pentru utilizator pentru eroarea: unul din cele N numere a fost introdus gresit*/
    public static final String BAD_NUMBER = " is not a valid number";
    /*cand variabila state are aceasta valoare, urmeaza sa citesc N de la tastatura*/
    public static final int GETTING_N = -1;
    /*cand variabila state are aceasta valoarea
    urmeaza sa citim primul dintre cele N numere*/
    public static final int GETTING_FIRST_NUMBER = 0;

    /*descrierea ce va fi afisata utilizatorului*/
    protected String description = InputDoubleArray.INPUT_N;
    /*starea curenta*/
    protected int state = InputDoubleArray.GETTING_N;
    /*N numere*/
    protected int n = 0;
    /*lista cu cele N numere*/
    protected ArrayList<Double> list = new ArrayList<>();

    /*returnam descrierea pentru utilizator catre ProgramHandler*/
    public String getDescription() {
        return description;
    }

    /*primim input de la utilizator in acest parametru de tip String*/
    public void run(String userInput) {
        if(state == InputDoubleArray.GETTING_N) { /*daca userul tocmai l-a introdus pe N*/
            handleGettingN(userInput); /*salvam N in variabila membru*/
        } else if(state < this.n) { /*daca mai avem de citit numere*/
            handleGettingNumber(userInput); /*adaugam numarul in lista*/
        }
        if(state == this.n) { /*daca tocmai am citit ultimul numar*/
            run(); /*rezolvarea propriu zisa a problemei*/
        }
    }

    private void handleGettingN(String userInput) {
        int n = 0;
        try {
            n = Integer.parseInt(userInput); //arunca exceptie in caz de eroare
            description = InputDoubleArray.INPUT_NUMBERS; //am citit N cu succes ==>
            state = InputDoubleArray.GETTING_FIRST_NUMBER; //trecem in starea in care citim cele N numere
            this.n = n; //salvam valoarea citita de la utilizator
        } catch (Exception e) { //daca userInput nu este un numar
            description = userInput + InputDoubleArray.BAD_N; //setam descrierea pentru eroare
        }
    }

    private void handleGettingNumber(String userInput) {
        double nextNumber = 0.0;
        try {
            nextNumber = Double.parseDouble(userInput); //arunca exceptie in caz de eroare
            description = InputDoubleArray.INPUT_NUMBERS; //am citit numarul cu succes
            list.add(nextNumber); //adaugam numarul in lista
            state++; //crestem variabila de stare pentru a reflecta faptul ca am mai citit un numar
        } catch(Exception e) { //daca userInput nu este un numar
            description = userInput + InputDoubleArray.BAD_NUMBER; //setam descrierea pentru eroare
        }
    }

    /*programele cu input un array de numere rationale pot mosteni din
    aceasta clasa si pot accesa variabila membru list in functia run
    Aceasta functie este apelata dupa ce este primit un set complet
    de date de la utilizator*/
    public abstract void run();
}
