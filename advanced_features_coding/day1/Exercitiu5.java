package advanced_features_coding.day1;

/*
Folosind clasele create la exercițiile anterioare creați un program care îi cere
utilizatorului un cuvânt, apoi determină dacă acel cuvânt este palindrom sau nu.
Un cuvânt este palindrom dacă se citește la fel și de la dreapta la stânga și de
la stânga la dreapta (ex: ana);
 */
public class Exercitiu5 implements Program {
    /*Afisam utilizatorului inainte sa-i cerem input*/
    public String getDescription() {
        return "Introdu un cuvant";
    }

    /*Procesam input de la utilizator si afisam rezultatul*/
    public void run(String userInput) {
        /*obtinem string-ul inversat*/
        String reverse = new StringBuilder(userInput).reverse().toString();
        if(reverse.equals(userInput)) { /*daca inversul este egal cu originalul*/
            System.out.println(userInput + " este palindrom");
        } else { /*daca inversul este diferit de original*/
            System.out.println(userInput + " nu este palindrom");
        }
    }
    public String toString() {return "Ziua 1 - exercitiul 5";}
}
