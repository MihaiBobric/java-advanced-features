package advanced_features_coding.day2;

/*
Clasa care masoara durata de executie a unei functii
Pentru a o folosi, extindeti aceasta clasa, iar in
clasa mostenitoare apelati functia a carei durata de
executie vreti sa o masurati din functia void run()
Instantiati clasa mostenitoare si apelati functia time()
Valoarea returnata este durata executiei functiei run()
masurata in nanosecunde
 */
public abstract class TimeFunction implements Runnable {
    public long time() {
        long before = System.nanoTime();
        this.run();
        long diff = System.nanoTime() - before;
        System.out.println("Testing " + this + " " + diff + " nanos"); /*afisam rezultatul*/
        return diff; /*returnam rezultatul*/
    }
}
