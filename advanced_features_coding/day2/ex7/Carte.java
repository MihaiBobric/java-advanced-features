package advanced_features_coding.day2.ex7;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Carte {
    public static SimpleDateFormat formatImplicit = new SimpleDateFormat("yyyy-MM-dd");
    private String titlu;
    private String autor;
    private String editura;
    private Date published;

    public Carte() {
        titlu = null;
        autor = null;
        editura = null;
        published = null;
    }

    public Carte(String titlu, String autor, String editura, Date published) {
        this.titlu = titlu;
        this.autor = autor;
        this.editura = editura;
        this.published = published;
    }

    public String getTitlu() {
        return titlu;
    }

    public void setTitlu(String titlu) {
        this.titlu = titlu;
    }

    public String getAutor() {
        return autor;
    }

    public String getEditura() {
        return editura;
    }

    public Date getPublished() {
        return published;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public void setEditura(String editura) {
        this.editura = editura;
    }

    public void setPublished(Date published) {
        this.published = published;
    }
    public String toString() {
        return titlu + "\n" +
                autor + "\n" +
                editura + "\n" +
                published;
    }
}
