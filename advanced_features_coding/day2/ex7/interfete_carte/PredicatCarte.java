package advanced_features_coding.day2.ex7.interfete_carte;

import advanced_features_coding.day2.ex7.Carte;

public interface PredicatCarte {
    boolean propozitie(Carte carte);
}
