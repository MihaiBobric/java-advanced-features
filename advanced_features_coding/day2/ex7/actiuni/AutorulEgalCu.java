package advanced_features_coding.day2.ex7.actiuni;

import advanced_features_coding.day2.ex7.Carte;
import advanced_features_coding.day2.ex7.interfete_carte.PredicatCarte;

public class AutorulEgalCu implements PredicatCarte {
    private String autor;
    public AutorulEgalCu(String autor) {
        this.autor = autor;
    }
    public boolean propozitie(Carte carte) {
        if(autor.equals(carte.getAutor())) {
            return true;
        }
        return false;
    }
}
