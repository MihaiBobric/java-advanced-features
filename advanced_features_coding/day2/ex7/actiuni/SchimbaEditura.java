package advanced_features_coding.day2.ex7.actiuni;

import advanced_features_coding.day2.ex7.Carte;
import advanced_features_coding.day2.ex7.interfete_carte.SchimbareCarte;

public class SchimbaEditura implements SchimbareCarte {
    private String editura;
    public SchimbaEditura(String editura) {
        this.editura = editura;
    }
    public void schimba(Carte carte) {
        carte.setEditura(editura);
    }
}
