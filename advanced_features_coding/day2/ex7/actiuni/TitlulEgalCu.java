package advanced_features_coding.day2.ex7.actiuni;

import advanced_features_coding.day2.ex7.Carte;
import advanced_features_coding.day2.ex7.interfete_carte.PredicatCarte;

public class TitlulEgalCu implements PredicatCarte {
    private String titlu;
    public TitlulEgalCu(String titlu) {
        this.titlu = titlu;
    }
    public boolean propozitie(Carte carte) {
        if(titlu.equals(carte.getTitlu())) {
            return true;
        }
        return false;
    }
}
