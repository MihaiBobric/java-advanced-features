package advanced_features_coding.day2.ex7.actiuni;

import advanced_features_coding.day2.ex7.Carte;
import advanced_features_coding.day2.ex7.interfete_carte.SchimbareCarte;

import java.util.Date;

public class SchimbaDataPublicatiei implements SchimbareCarte {
    private String published;
    public SchimbaDataPublicatiei(String published) {
        this.published = published;
    }
    public void schimba(Carte carte) {
        try {
            Date published = Carte.formatImplicit.parse(this.published);
            carte.setPublished(published);
        } catch(Exception e) {
            System.err.println("Nu-i buna data");
        }

    }
}
