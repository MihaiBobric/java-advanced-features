package advanced_features_coding.day2.ex7.actiuni;

import advanced_features_coding.day2.ex7.Carte;
import advanced_features_coding.day2.ex7.interfete_carte.PredicatCarte;

import java.util.Date;

public class DupaDataPublicatiei implements PredicatCarte {
    private Date published;

    public DupaDataPublicatiei(Date published) {
        this.published = published;
    }

    public boolean propozitie(Carte carte) {
        if(published.compareTo(carte.getPublished()) < 0) {
            return true;
        }
        return false;
    }
}
