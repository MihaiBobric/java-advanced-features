package advanced_features_coding.day2.ex7.meniuri.principal.cautari;

import advanced_features_coding.day2.ex7.Biblioteca;
import advanced_features_coding.day2.ex7.meniuri.principal.MeniuAfiseazaLista;
import advanced_features_coding.day2.menu_generics.BaseMenu;
import advanced_features_coding.day2.menu_generics.Menu;

public class MeniuCautaLaDataPublicatiei extends BaseMenu {
    Biblioteca app;
    Biblioteca rezultate;
    public MeniuCautaLaDataPublicatiei(Menu prev, Biblioteca app) {
        super(prev);
        this.app = app;
        this.rezultate = new Biblioteca();
    }
    public void draw() {
        System.out.println("Data:");
        rezultate = new Biblioteca(
                app.cautaLaDataPublicatiei(BaseMenu.input.nextLine())
        );
    }
    public Menu select() {
        return new MeniuAfiseazaLista(prev, rezultate);
    }

    public String toString() {
        return "La de data";
    }
}
