package advanced_features_coding.day2.ex7.meniuri.principal.cautari;

import advanced_features_coding.day2.ex7.Biblioteca;
import advanced_features_coding.day2.menu_generics.BaseMenu;
import advanced_features_coding.day2.menu_generics.Menu;

/*
Un meniu de cautare cu un criteriu concret (submeniurile acestui meniu)
returneaza un meniu de afisare a listei filtrate fara optiunile care
permit schimbarea sau stergerea cartii
La intoarcerea din respectivul meniu de afisare ajungem inapoi la acest meniu
*/
public class MeniuCautare extends BaseMenu {
    Biblioteca app;
    public MeniuCautare(Menu prev, Biblioteca app) {
        super(prev);
        this.app = app;
        addOption("titlu", new MeniuCautaDupaTitlu(this, app));
        addOption("autor", new MeniuCautaDupaAutor(this, app));
        addOption("editura", new MeniuCautaDupaEditura(this, app));
        addOption("data", new MeniuCautaDataPublicatiei(this, app));
    }

    public String toString() {
        return "Cauta carti";
    }
}
