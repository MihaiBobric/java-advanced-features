package advanced_features_coding.day2.ex7.meniuri.principal;

import advanced_features_coding.day2.ex7.Biblioteca;
import advanced_features_coding.day2.menu_generics.ActionMenu;
import advanced_features_coding.day2.menu_generics.BaseMenu;
import advanced_features_coding.day2.menu_generics.Menu;

public class MeniuIncarcaFisier extends ActionMenu {
    Biblioteca app;
    public MeniuIncarcaFisier(Menu prev, Biblioteca app) {
        super(prev);
        this.app = app;
    }
    public void run() {
        System.out.println("Numele fisierului:");
        app.incarca(BaseMenu.input.nextLine());
    }
    public String toString() {
        return "Incarca dintr-un fisier";
    }
}