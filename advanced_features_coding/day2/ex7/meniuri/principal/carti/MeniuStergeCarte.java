package advanced_features_coding.day2.ex7.meniuri.principal.carti;

import advanced_features_coding.day2.ex7.Biblioteca;
import advanced_features_coding.day2.menu_generics.*;

public class MeniuStergeCarte extends ActionMenu {
    Biblioteca app;
    int index;
    public MeniuStergeCarte(Menu prev, Biblioteca app, int index) {
        super(prev);
        this.app = app;
        this.index = index;
    }
    public void run() {
        app.sterge(index);
    }
    public String toString() {
        return "Sterge cartea";
    }
}
