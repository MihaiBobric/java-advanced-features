package advanced_features_coding.day2.ex7.meniuri.principal.carti.schimbari;

import advanced_features_coding.day2.ex7.Biblioteca;
import advanced_features_coding.day2.menu_generics.ActionMenu;
import advanced_features_coding.day2.menu_generics.BaseMenu;
import advanced_features_coding.day2.menu_generics.Menu;

public class MeniuSchimbaAutorul extends ActionMenu {
    private Biblioteca app;
    private int index;
    public MeniuSchimbaAutorul(Menu prev, Biblioteca app, int index) {
        super(prev);
        this.app = app;
        this.index = index;
    }
    public void run() {
        System.out.println("Autor:");
        app.schimbaAutorul(BaseMenu.input.nextLine(), index);
    }
    public String toString() {
        return "Schimba autorul";
    }

}
