package advanced_features_coding.day2.ex7.meniuri.principal;

import advanced_features_coding.day2.ex7.Biblioteca;
import advanced_features_coding.day2.ex7.meniuri.principal.carti.MeniuAdaugaCarte;
import advanced_features_coding.day2.ex7.meniuri.principal.carti.MeniuDetaliiCarte;
import advanced_features_coding.day2.ex7.meniuri.principal.carti.MeniuDetaliiCarteExtra;
import advanced_features_coding.day2.menu_generics.BaseMenu;
import advanced_features_coding.day2.menu_generics.Menu;

/*
Afiseaza lista sub forma unor submeniuri
Accesarea fiecarui submeniu corespunzator unei carti
afiseaza detaliile cartii si o serie de operatii asupra cartii
*/
public class MeniuAfiseazaListaExtra extends BaseMenu {
    Biblioteca app;
    Menu meniuAdaugare;
    public MeniuAfiseazaListaExtra(Menu prev, Biblioteca app) {
        super(prev);
        this.app = app;
        meniuAdaugare = new MeniuAdaugaCarte(this, app);
    }
    public void draw() {
        removeCustomOptions();
        for(int i = 0; i < app.numarCarti(); ++i) {
            addOption(Integer.toString(i), new MeniuDetaliiCarteExtra(this, app, i));
        }
        addOption("adauga", meniuAdaugare);
        super.draw();
    }
    public String toString() {
        return "Afiseaza cartile";
    }
}
