package advanced_features_coding.day2.ex7.meniuri.principal;

import advanced_features_coding.day2.ex7.Biblioteca;
import advanced_features_coding.day2.menu_generics.ActionMenu;
import advanced_features_coding.day2.menu_generics.BaseMenu;
import advanced_features_coding.day2.menu_generics.Menu;

public class MeniuSalveazaFisier extends ActionMenu {
    Biblioteca app;
    public MeniuSalveazaFisier(Menu prev, Biblioteca app) {
        super(prev);
        this.app = app;
    }
    public void run() {
        System.out.println("Numele fisierului:");
        app.salveaza(BaseMenu.input.nextLine());
    }
    public String toString() {
        return "Salveaza intr-un fisier";
    }
}