package advanced_features_coding.day2.ex7.meniuri;

import advanced_features_coding.day2.ex7.Biblioteca;
import advanced_features_coding.day2.ex7.meniuri.principal.MeniuAfiseazaListaExtra;
import advanced_features_coding.day2.ex7.meniuri.principal.MeniuIncarcaFisier;
import advanced_features_coding.day2.ex7.meniuri.principal.MeniuSalveazaFisier;
import advanced_features_coding.day2.ex7.meniuri.principal.carti.MeniuAdaugaCarte;
import advanced_features_coding.day2.ex7.meniuri.principal.MeniuAfiseazaLista;
import advanced_features_coding.day2.ex7.meniuri.principal.cautari.MeniuCautare;
import advanced_features_coding.day2.menu_generics.BaseMenu;
/*
Structura meniu:
Adauga o carte
Afiseaza cartile
	Detalii carte
		Schimba titlul
		Schimba autorul
		Schimba editura
		Schimba data publicatiei
		Sterge cartea
		Meniu anterior
		Quit
Cauta
	Dupa titlu
	Dupa autor
	Dupa editura
	Dupa data publicatiei
		La data de
		Inainte de data de
		Dupa data de
		Meniu anterior
		Quit
	Meniu anterior
	Quit
Salveaza
Incarca
Quit
 */
public class MeniuPrincipal extends BaseMenu {
    Biblioteca app;
    public MeniuPrincipal(Biblioteca app) {
        this.app = app;
        addOption("adauga", new MeniuAdaugaCarte(this, app));
        addOption("afiseaza", new MeniuAfiseazaListaExtra(this, app));
        addOption("salveaza", new MeniuSalveazaFisier(this, app));
        addOption("incarca", new MeniuIncarcaFisier(this, app));
        addOption("cauta", new MeniuCautare(this, app));


    }

    public String toString() {
        return "Gestiune carti";
    }
}
