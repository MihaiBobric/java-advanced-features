package advanced_features_coding.day2.menu_generics;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;
/*
Clasa in care implementam functionalitatile comune ale meniurilor
 */
public class BaseMenu implements Menu {
    /*pentru optiunea utilizatorului*/
    static public Scanner input = new Scanner(System.in);
    /*pentru a putea cauta submeniurile dupa optiunea pe care
    trebuie sa o introduca utilizatorul le stocam intr-un
    HashMap folosind optiunea ca si cheie*/
    private HashMap<String, Menu> options = new HashMap<String, Menu>();
    /*tinem minte cea mai lunga optiune pentru pretty printing*/
    private int longestOption = 0;

    /*referinta la meniul anterior pentru optiunea "p"*/
    protected Menu prev;

    /*Constructor implicit: nu avem meniu anterior*/
    public BaseMenu() {
        this(null);
    }

    public BaseMenu(Menu prev) {
        if(prev != null) { /*daca avem meniu anterior*/
            this.prev = prev; /*il stocam in variabila membru*/
            addOption("p", prev); /*si adaugam optiunea "p"*/
        }
        addOption("q", new QuitMenu()); /*adaugam optiunea de iesire*/
    }

    public void draw() {
        /*afisam numele meniului curent*/
        System.out.println(this + ": ");
        /*obtinem un iterator pentru parcurgerea optiunilor*/
        Iterator it = options.entrySet().iterator();
        while(it.hasNext()) { /*cat timp mai avem optiuni*/
            Map.Entry pair = (Map.Entry)it.next();
            System.out.print(pair.getKey()); /*afisam optiunea curenta*/
            for(int i = pair.getKey().toString().length(); i < longestOption; ++i) {
                /*afisam spatii pana la lungimea optiunii mai lungi*/
                System.out.print(" ");
            }
            /*afisam numele submeniului coresunzator optiunii curente*/
            System.out.println(" -- " + pair.getValue());
        }
    }

    public Menu select() {
        String option; /*aici stocam optiunea de la utilizator*/
        do {
            /*afisam o descriere utilizatorului*/
            System.out.println("Choose an option!");
            try {
                option = input.nextLine(); /*citim optiunea*/
            } catch(Exception e) { /*daca intampinam o eroare*/
                return getOption("q"); /*returnam meniul de iesire*/
            }
        } while(!options.containsKey(option)); /*repeta citirea optiunii pana este o
                                                optiune valida*/
        return getOption(option); /*returnam submeniul corespunzator optiunii alese*/
    }

    /*pentru a adauga o optiune la meniu*/
    public void addOption(String key, Menu ref) {
        if(longestOption < key.length()) {/*daca noua optiune este cea mai lunga*/
            longestOption = key.length();/*actualizam lungimea celei
                                            mai lungi optiuni*/
        }
        options.put(key, ref); /*adaugam noua optiune la meniu*/
    }
    /*pentru a adauga un submeniu cu o optiune numerica implicita*/
    public void addOption(Menu ref) {
        addOption(Integer.toString(options.size()), ref);
    }

    /*cautare submeniu dupa optiune*/
    public Menu getOption(String key) {
        return options.get(key);
    }
    /*cautare submeniu dupa optiune numerica*/
    public Menu getOption(int key) {
        return options.get(Integer.toString(key));
    }

    /*elimina toate optiunile mai putin "p" si "q"*/
    public void removeCustomOptions() {
        Menu q = options.get("q");
        removeAllOptions();
        if(prev != null) {
            addOption("p", prev);
        }
        addOption("q", q);
    }

    /*elimina toate optiunile*/
    public void removeAllOptions() {
        options.clear();
    }

}
