package advanced_features_coding.day2.menu_generics;
/*
Clasa care controleaza un meniu
 */
public class MenuHandler {
    Menu currentMenu; /*stocam o referinta catre meniul curent*/
    public MenuHandler(Menu currentMenu) { /*primim meniul de inceput*/
        this.currentMenu = currentMenu;
    }
    public void start() {
        while(currentMenu != null) { /*cat timp avem un meniu*/
            currentMenu.draw(); /*il afisam*/
            currentMenu = currentMenu.select(); /*obtinem meniul urmator*/
        }
    }
}
