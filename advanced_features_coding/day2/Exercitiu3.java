package advanced_features_coding.day2;

import advanced_features_coding.day2.timed_tests.*;

public class Exercitiu3 {
    public void run() {
        int n = 1000; //de cate ori se efectueaza un test
        ListTest.INITIAL_SIZE = 1000; //dimensiunea listei inainte de test
        ArrayListGetTest test1 = new ArrayListGetTest(n);
        test1.time();
        LinkedListGetTest test2 = new LinkedListGetTest(n);
        test2.time();
        ArrayListAddTest test3 = new ArrayListAddTest(n);
        test3.time();
        LinkedListAddTest test4 = new LinkedListAddTest(n);
        test4.time();
        LinearTest test5 = new LinearTest(10000);
        test5.time();
        QuadraticTest test6 = new QuadraticTest(10000);
        test6.time();
    }
}
