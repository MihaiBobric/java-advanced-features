package advanced_features_coding.day2.print_char_types;

import advanced_features_coding.day2.PrintChar;

public class TChar implements PrintChar {
    public void print() {
        System.out.print("T");
    }
}
