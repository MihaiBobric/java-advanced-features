package advanced_features_coding.day2.timed_tests;

import advanced_features_coding.day2.TimeFunction;

/*Clasa care masoara durata parcurgerii unei matrici patrate cu dimensiune configurabila*/
public class QuadraticTest extends TimeFunction {
    private long len;
    public QuadraticTest(long len) {
        this.len = len;
    }
    public void run() {
        for(long i = 0; i < len; ++i) {
            for(long j = 0; j < len; ++j) {}
        }
    }
    public String toString() {
        return "Quadratic " + len + "\t";
    }
}
