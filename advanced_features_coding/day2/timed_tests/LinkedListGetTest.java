package advanced_features_coding.day2.timed_tests;

import java.util.LinkedList;

/*GetTest pe un LinkedList*/
public class LinkedListGetTest extends GetTest {
    public LinkedListGetTest(int times) { super(new LinkedList<>(), times); }
    public String toString() { return "LinkedListGetTest(" + times + ")"; }
}
