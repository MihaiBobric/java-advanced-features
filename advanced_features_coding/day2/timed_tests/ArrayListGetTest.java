package advanced_features_coding.day2.timed_tests;

import java.util.ArrayList;

/*GetTest pe un ArrayList*/
public class ArrayListGetTest extends GetTest {
    public ArrayListGetTest(int times) { super(new ArrayList<>(), times); }
    public String toString() { return "ArrayListGetTest(" + times + ") "; }
}
