package advanced_features_coding.day2.timed_tests;

import advanced_features_coding.day2.TimeFunction;

import java.util.List;

/*
Clasa care masoara durata unei operatii care implica o lista
de numere intregi cu o dimensiune initiala configurabila
 */
public abstract class ListTest extends TimeFunction {
    public static int INITIAL_SIZE = 1000;
    protected List<Integer> list; /*lista*/
    public ListTest(List<Integer> list) { /*primim o referinta la un tip specific de lista*/
        this.list = list;
        for(int i = 0; i < INITIAL_SIZE; ++i) { /*adaugam elementele intiale*/
            list.add(0);
        }
    }
}
