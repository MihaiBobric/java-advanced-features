package advanced_features_coding.day2.timed_tests;

import java.util.List;

/*
Clasa care masoara durata accesarii elementului din
mijlocul unei liste in mod repetat cu un numar
configurabil de repetitii
 */
public class GetTest extends ListTest {
    protected int times; /*numarul de repetitii*/
    protected int pos; /*pozitia elementului din mijlocul listei*/
    public GetTest(List<Integer> list, int times) {
        super(list);
        this.times = times;
        this.pos = this.list.size() - 2;
    }
    public void run() {
        for(int i = 0; i < times; ++i) {
            list.get(pos);
        }
    }
}
