package advanced_features_coding.day2.ex5;

import advanced_features_coding.day1.*;
import advanced_features_coding.day2.menu_generics.BaseMenu;

public class MeniuPrincipal extends BaseMenu {
    public MeniuPrincipal() {
        /*adaugam submeniuri pentru fiecare program din day1*/
        addOption(new MeniuExecutaProgram(this, new Exercitiu3()));
        addOption(new MeniuExecutaProgram(this, new Exercitiu4()));
        addOption(new MeniuExecutaProgram(this, new Exercitiu5()));
        addOption(new MeniuExecutaProgram(this, new Exercitiu6()));
        addOption(new MeniuExecutaProgram(this, new Exercitiu7()));
        addOption(new MeniuExecutaProgram(this, new Exercitiu8()));
        addOption(new MeniuExecutaProgram(this, new Exercitiu9()));
        addOption(new MeniuExecutaProgram(this, new Exercitiu10()));
        addOption(new MeniuExecutaProgram(this, new Exercitiu11()));
        addOption(new MeniuExecutaProgram(this, new Exercitiu12()));
    }
    /*numele meniului*/
    public String toString() {
        return "Exercitii din ziua 1";
    }
}
