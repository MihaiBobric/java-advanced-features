package advanced_features_coding.day2.ex5;

import advanced_features_coding.day1.Program;
import advanced_features_coding.day1.ProgramHandler;
import advanced_features_coding.day2.menu_generics.BaseMenu;
import advanced_features_coding.day2.menu_generics.Menu;
/*
Clasa care ia un Program din exercitiile din prima zi si il executa
 */
public class MeniuExecutaProgram extends BaseMenu {
    protected Program program; /*programul de executat*/
    /*primim ca parametri meniul principal si programul de executat*/
    public MeniuExecutaProgram(Menu prev, Program program) {
        super(prev);
        this.program = program;
    }
    /*in pasul de desenare a meniului vom executa programul primit ca parametru*/
    public void draw() {
        /*instantiem un handler pentru program*/
        ProgramHandler handler = new ProgramHandler(program);
        /*executam programul*/
        handler.start();
    }
    public Menu select() {
        return prev; /*returnam meniul principal*/
    }
    /*numele meniului va fi numele programului primit ca parametru*/
    public String toString() {
        return program.toString();
    }
}
