package advanced_features_coding.day2.ex6;

import advanced_features_coding.day2.menu_generics.ActionMenu;
import advanced_features_coding.day2.menu_generics.BaseMenu;
import advanced_features_coding.day2.menu_generics.Menu;
/*
Citim numele fisierului de la utilizator
Incarcam lista din fisier
Ne intoarcem la meniul anterior
 */
public class MeniuIncarcaFisier extends ActionMenu {
    HandlerListaNumere app;
    public MeniuIncarcaFisier(Menu prev, HandlerListaNumere app) {
        super(prev);
        this.app = app;
    }
    public void run() {
        System.out.println("Numele fisierului:");
        app.incarca(BaseMenu.input.nextLine());
    }
    public String toString() {
        return "Incarca dintr-un fisier";
    }
}