package advanced_features_coding.day2.ex6;

import advanced_features_coding.day2.menu_generics.ActionMenu;
import advanced_features_coding.day2.menu_generics.BaseMenu;
import advanced_features_coding.day2.menu_generics.Menu;
/*
Citim de la utilizator pozitia numarului pe care sa-l stergem
Stergem numarul din lista
Ne intoarcem la meniul principal
*/
public class MeniuStergeNumar extends ActionMenu {
    HandlerListaNumere app;
    public MeniuStergeNumar(Menu prev, HandlerListaNumere app) {
        super(prev);
        this.app = app;
    }
    public void run() {
        System.out.println("Introdu pozitia numarului pe care vrei sa-l stergi");
        app.stergeNumar(BaseMenu.input.nextInt());
        BaseMenu.input.nextLine(); //functia nextInt lasa
                                //caracterul newline "\n"
                                //in consola si urmatorul apel
                                //de nextLine() va returna "\n"
    }
    public String toString() {
        return "Sterge un numar";
    }
}
