package advanced_features_coding.day2.ex6;

import advanced_features_coding.day2.menu_generics.MenuHandler;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Scanner;

/*
Clasa care prezinta un meniu utilizatorului in care
acesta poate sa gestioneze o lista de numere
Utilizatorul poate:
    sa adauge un numar la lista
    sa afiseze lista de numere
    sa schimbe un numar din lista
    sa stearga un numar din lista
    sa salveze lista intr-un fisier
    sa incarce lista dintr-un fisier
    sa afiseze suma numerelor
    sa afiseze media numerelor
    sa afiseze cel mai mare numar din lista
    sa afiseze cel mai mic numar din lista
    sa afiseze lista sortata crescator
    sa afiseze lista sortata descrescator
 */
public class HandlerListaNumere {
    /*handler pentru meniul aplicatiei; incepem cu meniul principal*/
    public MenuHandler handler = new MenuHandler(new MeniuPrincipal(this));
    /*lista cu numere*/
    private ArrayList<Integer> lista = new ArrayList<>();
    /*pentru adaugarea unui numar in lista*/
    public void adaugaNumar(int nr) {lista.add(nr);}
    /*pentru afisarea unei liste de numere (folosita intern)*/
    private void afiseazaLista(ArrayList<Integer> lista) {
        for(int i = 0; i < lista.size(); ++i) {
            System.out.println(lista.get(i));
        }
    }
    /*pentru afisarea listei*/
    public void afiseazaLista() {afiseazaLista(lista);}
    /*pentru stergerea unui numar dupa index*/
    public void stergeNumar(int index) {
        if(index < 0 || index >= lista.size()) { /*daca index ar fi in afara listei*/
            System.err.println("Nu exista pozitia " + index); /*afisam o eroare*/
        } else { /*daca index este corect*/
            lista.remove(index); /*stergem elementul*/
        }
    }
    /*pentru stergerea unui numar de la un index specific*/
    public void schimbaNumar(int index, int numar) {
        if(index < 0 || index >= lista.size()) { /*daca index ar fi in afara listei*/
            System.err.println("Nu exista pozitia " + index); /*afisam o eroare*/
        } else { /*daca index este corect*/
            lista.set(index, numar); /*setam noua valoare*/
        }
    }
    /*pentru salvarea listei intr-un fisier*/
    public void salveaza(String numeFisier) {
        try {
            /*incercam sa deschidem fisierul pentru scriere*/
            BufferedWriter out = new BufferedWriter(new FileWriter(numeFisier));
            /*daca am ajuns aici am reusit sa deschidem fisierul*/
            for(int i = 0; i < lista.size(); ++i) { /*parcurgem lista*/
                /*afisam si in consola pentru a verifica
                mai usor functionarea corecta*/
                System.out.print(lista.get(i).toString() + " ");
                /*scriem numarul in fisier*/
                out.write(lista.get(i).toString() + " ");
            }
            /*trecem la randul urmator in fisier*/
            out.write("\n");
            /*inchidem fisierul*/
            out.close();
        } catch(Exception e) { /*daca nu am reusit sa deschidem fisierul*/
            System.err.println(numeFisier + " nu exista"); /*afisam o eroare*/
        }
    }
    /*pentru incarcarea listei dintr-un fisier*/
    public void incarca(String numeFisier) {
        try {
            /*incercam sa deschidem fisierul pentru citire*/
            Scanner in = new Scanner(new BufferedReader(new FileReader(numeFisier)));
            /*daca am ajuns aici am reusit sa deschidem fisierul*/
            lista.clear(); /*stergem numerele vechi*/
            while(in.hasNextInt()) { /*cat timp mai sunt numere necitite din fisier*/
                /*citim un numar din fisier si il adaugam la lista*/
                lista.add(in.nextInt());
            }
        } catch(Exception e) { /*daca nu am reusit sa deschidem fisierul*/
            System.err.println(numeFisier + " nu exista"); /*afisam o eroare*/
        }
    }
    /*pentru calculul sumei numerelor*/
    public int suma() {
        int s = 0; /*incepem de la 0*/
        for(int i = 0; i < lista.size(); ++i) { /*parcurgem lista*/
            s += lista.get(i); /*adaugam numarul curent la suma*/
        }
        return s; /*returnam suma*/
    }
    /*pentru calculul mediei numerelor*/
    public double media() {
        return ((double)suma()) / lista.size();
    }
    /*pentru calculul celui mai mare numar*/
    public int max() {
        int m = lista.get(0); /*presupun ca maximul este primul element*/
        for(int i = 1; i < lista.size(); ++i) { /*parcurgem lista*/
            /*daca elementul curent este mai mare decat maximul presupus*/
            if(m < lista.get(i)) {
                m = lista.get(i); /*setam noul maxim*/
            }
        }
        return m; //returnam maximul gasit
    }
    /*pentru calculul celui mai mic numar*/
    public int min() {
        int m = lista.get(0); /*presupun ca minimul este primul element*/
        for(int i = 1; i < lista.size(); ++i) { /*parcurgem lista*/
            /*daca elementul curent este mai mic decat minimul presupus*/
            if(m > lista.get(i)) {
                m = lista.get(i); /*setam noul minim*/
            }
        }
        return m; //returnam minimul gasit
    }
    /*pentru afisarea crescatoare sau descrescatoare a listei*/
    public void afisareSortata(boolean crescator) {
        /*clonam lista pentru a o sorta*/
        ArrayList<Integer> sortata = (ArrayList<Integer>)lista.clone();
        /*functia sort primeste ca parametru instanta unei clase care
        * implementeaza interfata Comparator; aceasta interfata are o
        * functie compare care primeste doua obiecte si returneaza
        * ceva mai mare decat 0 daca primul obiect este mai mare
        * decat al doilea, 0 daca obiectele sunt egale si ceva
        * mai mic decat 0 daca al doilea obiect este mai mare
        * decat primul conform unui criteriu oarecare*/
        if(crescator) {
            sortata.sort(new Comparator<Integer>() {
                /*pentru sortarea crescatoare a unei liste de numere
                putem folosi ca si criteriu diferenta dintre numere*/
                public int compare(Integer t1, Integer t2) {
                    return t1 - t2;
                }
            });
        } else {
            sortata.sort(new Comparator<Integer>() {
                /*pentru sortarea crescatoare a unei liste de numere
                putem folosi ca si criteriu opusul diferentei dintre numere*/
                public int compare(Integer t1, Integer t2) {
                    return t2 - t1;
                }
            });
        }
        /*afisam lista sortata*/
        afiseazaLista(sortata);
    }
}
