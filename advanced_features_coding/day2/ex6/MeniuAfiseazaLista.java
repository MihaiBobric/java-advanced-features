package advanced_features_coding.day2.ex6;

import advanced_features_coding.day2.menu_generics.ActionMenu;
import advanced_features_coding.day2.menu_generics.BaseMenu;
import advanced_features_coding.day2.menu_generics.Menu;

/*Afisam lista si ne intoarcem la meniul principal*/
public class MeniuAfiseazaLista extends ActionMenu {
    HandlerListaNumere app;
    public MeniuAfiseazaLista(Menu prev, HandlerListaNumere app) {
        super(prev);
        this.app = app;
    }
    public void run() {
        app.afiseazaLista();
    }
    public String toString() {
        return "Afiseaza lista";
    }
}
