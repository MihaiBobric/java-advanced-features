package advanced_features_coding.day2.ex4;

import advanced_features_coding.day2.menu_generics.BaseMenu;
import advanced_features_coding.day2.menu_generics.Menu;
/*
Clasa care implementeaza interfata unui meniu dar atunci
cand este selectat afiseaza un text si returneaza meniul anterior
 */
public class MeniuAfiseazaText extends BaseMenu {
    String text; /*textul pe care il afisam*/
    String nume; /*numele meniului*/

    /*primim ca parametri:
    * meniul anterior
    * numele acestui meniu
    * textul pe care il afisam
    */
    public MeniuAfiseazaText(Menu prev, String nume, String text) {
        super(prev);
        this.text = text;
        this.nume = nume;
    }
    public void draw() { /*afisam textul*/
        System.out.println(text);
    }
    public Menu select() { /*returnam meniul anterior*/
        return prev;
    }
    public String toString() { return nume; }
}
