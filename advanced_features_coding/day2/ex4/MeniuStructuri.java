package advanced_features_coding.day2.ex4;

import advanced_features_coding.day2.menu_generics.BaseMenu;
import advanced_features_coding.day2.menu_generics.Menu;

public class MeniuStructuri extends BaseMenu {
    public MeniuStructuri(Menu prev) {
        super(prev);
        /*lista exercitiilor cu structuri de date*/
        addOption("1", new MeniuAfiseazaText(this, "Problema 1",
                "Enuntul primei probleme cu structuri de date"));
        addOption("2", new MeniuAfiseazaText(this, "Problema 2",
                "Enuntul celei de-a doua probleme cu structuri de date"));
    }
    /*numele meniului*/
    public String toString() {
        return "Structuri de date";
    }
}
