package advanced_features_coding.day2.ex4;

import advanced_features_coding.day2.menu_generics.BaseMenu;
import advanced_features_coding.day2.menu_generics.Menu;

public class MeniuCalcul extends BaseMenu {
    public MeniuCalcul(Menu prev) {
        super(prev);
        /*lista problemelor de calcul*/
        addOption("1", new MeniuAfiseazaText(this, "Problema 1",
                "Enuntul primei probleme de calcul"));
        addOption("2", new MeniuAfiseazaText(this, "Problema 2",
                "Enuntul celei de-a doua probleme de calcul"));
    }
    /*numele meniului*/
    public String toString() {
        return "Probleme de calcul";
    }
}
