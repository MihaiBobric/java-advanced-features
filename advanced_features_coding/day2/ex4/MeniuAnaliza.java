package advanced_features_coding.day2.ex4;

import advanced_features_coding.day2.menu_generics.BaseMenu;
import advanced_features_coding.day2.menu_generics.Menu;

public class MeniuAnaliza extends BaseMenu {
    public MeniuAnaliza(Menu prev) {
        super(prev);
        /*lista problemelor de analiza*/
        addOption("1", new MeniuAfiseazaText(this, "Problema 1",
                "Enuntul primei probleme de analiza"));
        addOption("2", new MeniuAfiseazaText(this, "Problema 2",
                "Enuntul celei de-a doua probleme de analiza"));
    }
    /*numele meniului*/
    public String toString() {
        return "Analiza functiilor";
    }
}
