package advanced_features_coding.day2.ex4;

import advanced_features_coding.day2.menu_generics.BaseMenu;
import advanced_features_coding.day2.menu_generics.Menu;

public class MeniuLogica extends BaseMenu {
    public MeniuLogica(Menu prev) {
        super(prev);
        /*lista problemelor de logica*/
        addOption("1", new MeniuAfiseazaText(this, "Problema 1",
                "Enuntul primei probleme de logica"));
        addOption("2", new MeniuAfiseazaText(this, "Problema 2",
                "Enuntul celei de-a doua probleme de logica"));
    }
    /*numele meniului*/
    public String toString() {
        return "Logica";
    }
}
