package advanced_features_coding.day2;
import advanced_features_coding.day2.print_char_types.*;

public class Exercitiu2 {
    /*reprezentarea patratului*/
    PrintChar[][] map;

    public void run() {
        /*schimba aceasta functie pentru a obtine un patrat diferit*/
        /*cu una din exemplele de mai jos */
        generateNxMQChars(5, 15);
        /*afisam patratul*/
        for(int i = 0; i < map.length; ++i) { /*pentru fiecare rand*/
            for(int j = 0; j < map[i].length; ++j) { /*pentru fiecare coloana*/
                map[i][j].print(); /*afisam elementul curent*/
            }
            System.out.println(); /*trecem cu afisarea la randul urmator*/
        }
    }
    public void generate20x20QChars() {
        map = new PrintChar[20][20]; /*alocam memorie pentru 20x20 elemente*/
        for(int i = 0; i < map.length; ++i) { /*pe fiecare rand*/
            for(int j = 0; j < map[i].length; ++j) { /*pe fiecare coloana*/
                map[i][j] = new QChar(); /*punem un QChar*/
            }
        }
    }
    public void generate20x20RandomChars() {
        map = new PrintChar[20][20]; /*alocam memorie pentru 20x20 elemente*/
        for(int i = 0; i < map.length; ++i) { /*pe fiecare rand*/
            for(int j = 0; j < map[i].length; ++j) { /*pe fiecare coloana*/
                double chance = Math.random(); /*generam un numar aleator intre 0 si 1*/
                if(chance < 0.05) { //5%
                    map[i][j] = new QChar();
                } else if(chance < 0.1) { //5%
                    map[i][j] = new TChar();
                } else { //90%
                    map[i][j] = new EmptyChar();
                }
            }
        }
    }
    public void generateChessBoard() {
        map = new PrintChar[8][8]; /*alocam memorie pentru 8x8 elemente*/
        //asezam piesele jucatorului negru
        map[0][0] = new Turn();
        map[0][1] = new Cal();
        map[0][2] = new Nebun();
        map[0][3] = new Regina();
        map[0][4] = new Rege();
        map[0][5] = new Nebun();
        map[0][6] = new Cal();
        map[0][7] = new Turn();
        for(int i = 0; i < map[1].length; ++i) { /*asezam pionii*/
            map[1][i] = new Pion();
        }
        //asezam spatiile libere
        for(int i = 2; i < 6; ++i) {
            for(int j = 0; j < map[i].length; ++j) {
                map[i][j] = new EmptyChar();
            }
        }
        //asezam piesele jucatorului alb
        for(int i = 0; i < map[6].length; ++i) { /*asezam pionii*/
            map[6][i] = new Pion();
        }
        map[7][0] = new Turn();
        map[7][1] = new Cal();
        map[7][2] = new Nebun();
        map[7][3] = new Rege();
        map[7][4] = new Regina();
        map[7][5] = new Nebun();
        map[7][6] = new Cal();
        map[7][7] = new Turn();
    }
    public void generateNxMQChars(int n, int m) {
        map = new PrintChar[n][m]; /*alocam memorie pentru n x m elemente*/
        for(int i = 0; i < map.length; ++i) { /*pe fiecare rand*/
            for(int j = 0; j < map[i].length; ++j) { /*pe fiecare coloana*/
                map[i][j] = new QChar(); /*punem un QChar*/
            }
        }
    }
}
