package advanced_features_coding.day3.ordonate;

import advanced_features_coding.day3.liste.DynamicArray;

public class Stiva implements StructuraOrdonata {
    private DynamicArray data = new DynamicArray();
    public boolean isEmpty() {
        return data.size() == 0;
    }

    public void push(Object elem) {
        data.adaugaElement(elem);
    }

    public Object pop() {
        Object rez = data.get(data.size() - 1);
        data.stergeElement(data.size() - 1);
        return rez;
    }

    public Object top() {
        return data.get(data.size() - 1);
    }

    public void list() {
        data.afiseazaListaElemente();
    }
}
