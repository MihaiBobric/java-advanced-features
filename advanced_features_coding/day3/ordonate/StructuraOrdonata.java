package advanced_features_coding.day3.ordonate;

public interface StructuraOrdonata {
    boolean isEmpty();
    void push(Object elem);
    Object pop();
    Object top();
    void list();
}
