package advanced_features_coding.day3.liste;

import advanced_features_coding.day3.generics.ActiuneElement;
import advanced_features_coding.day3.generics.PredicatElement;

public class DynamicArray implements Lista {
    public static int MIN_SIZE = 15;
    protected Object[] data = new Object[MIN_SIZE];
    protected int allocated = MIN_SIZE;
    protected int size = 0;

    private void alocaMemorie() {
        if(size + 1 < allocated) {
            return;
        }
        Object[] oldData = data;
        allocated = allocated * 2 + 1;
        data = new Object[allocated];
        for(int i = 0; i < oldData.length; ++i) {
            data[i] = oldData[i];
        }
        System.out.println("Allocated: " + allocated);
    }

    public void adaugaElement(Object e) {
        adaugaElement(e, size);
    }

    public void adaugaElement(Object e, int index) {
        if(index > size || index < 0) {
            throw new ArrayIndexOutOfBoundsException();
        }
        if(size + 1 >= allocated) {
            alocaMemorie();
        }
        for(int i = size; i > index; --i) {
            data[i] = data[i - 1];
        }
        data[index] = e;
        size++;
    }

    public boolean areElement(Object e) {
        for(int i = 0; i < size; ++i) {
            if(e.equals(data[i])) {
                return true;
            }
        }
        return false;
    }

    public void stergeElement(Object e) {
        boolean gasit = false;
        for(int i = 0; i < size; ++i) {
            if(gasit == false && e.equals(data[i])) {
                gasit = true;
                size--;
                if(i == size) break;
            }
            if(gasit) {
                data[i] = data[i + 1];
            }
        }
    }

    public void stergeElement(int index) {
        if(index >= size || index < 0) {
            throw new ArrayIndexOutOfBoundsException();
        }
        size--;
        for(int i = index; i < size; ++i) {
            data[i] = data[i + 1];
        }
    }

    public void afiseazaListaElemente() {
        System.out.print("[");
        parcurge(new ActiuneElement() {
            public void actiune(Object e) {
                System.out.print(e + " ");
            }
        });
        System.out.println("]");
    }

    public void parcurge(ActiuneElement actiune) {
        for(int i = 0; i < size; ++i) {
            actiune.actiune(data[i]);
        }
    }

    public void ataseaza(Lista sursa) {
        sursa.parcurge(new ActiuneElement() {
            public void actiune(Object e) {
                adaugaElement(e);
            }
        });
    }

    public void ataseaza(Lista sursa, int index) {
        if(index > size || index < 0) {
            throw new ArrayIndexOutOfBoundsException();
        }
        Object[] restul = new Object[size - index];
        int j = 0;
        int k = 0;
        for(int i = index; i < size; ++i) {
            restul[k+j] = data[i];
            if(j < sursa.size()) {
                data[i] = sursa.get(j);
                j++;
            } else if(k < restul.length) {
                data[i] = restul[k];
                k++;
            }
        }
        for(; j < sursa.size(); ++j) {
            adaugaElement(sursa.get(j));
        }
        for(; k < restul.length; ++k) {
            adaugaElement(restul[k]);
        }
    }

    public Lista filtreaza(PredicatElement predicat) {
        DynamicArray rez = new DynamicArray();
        parcurge(new ActiuneElement() {
            public void actiune(Object e) {
                if(predicat.propozitie(e)) {
                    rez.adaugaElement(e);
                }
            }
        });
        return rez;
    }

    public Object get(int index) {
        if(index >= size || index < 0) {
            throw new ArrayIndexOutOfBoundsException();
        }
        return data[index];
    }

    public int size() {
        return size;
    }

    public boolean isEmpty() {
        return size == 0;
    }
}
