package advanced_features_coding.day3.liste;

import advanced_features_coding.day3.generics.ActiuneElement;
import advanced_features_coding.day3.generics.PredicatElement;

public class LinkedList implements Lista {
    private Element head = null;
    private Element tail = null;
    private int size = 0;

    public void adaugaElement(Object e) {
        Element item = new Element(e, null, null);
        if(size == 0) {
            head = item;
            tail = item;
        } else {
            item.prev = tail;
            tail.next = item;
            tail = item;
        }
        size++;
        //System.out.println("Adaugl@"+e+"@"+item+"@"+this);
    }

    public void adaugaElement(Object e, int index) {
        if(index > size || index < 0) {
            throw new ArrayIndexOutOfBoundsException();
        }
        if(size == 0 || index == size) {
            adaugaElement(e);
            return;
        }
        Element item = new Element(e, null, null);
        if(index == 0) {
            item.next = head;
            head.prev = item;
            head = item;
            //System.out.println("Adaug0@"+e+"@"+item+"@"+this);
            size++;
            return;
        }
        Element current = getInsideElement(index);
        if(current.prev != null) {
            current.prev.next = item;
            item.prev = current.prev;
        }
        current.prev = item;
        item.next = current;
        size++;
        //System.out.println("Adaug" + index + "@"+e+"@"+item+"@"+this);
    }

    public boolean areElement(Object e) {
        Element item = head;
        while(item != null) {
            if(item.data.equals(e)) {
                return true;
            }
            item = item.next;
        }
        return false;
    }

    public void stergeElement(Object e) {
        Element item = head;
        while(item != null) {
            if(item.data.equals(e)) {
                if(item.prev != null) {
                    item.prev.next = item.next;
                }
                if(item.next != null) {
                    item.next.prev = item.prev;
                }
                size--;
                return;
            }
            item = item.next;
        }
    }

    public void stergeElement(int index) {
        if(index >= size || index < 0) {
            throw new ArrayIndexOutOfBoundsException();
        }
        if(index == size - 1) {
            tail = tail.prev;
        }
        if(index == 0) {
            head = head.next;
        } else {
            Element current = getInsideElement(index);
            current.prev.next = current.next;
            current.next.prev = current.prev;
        }
        size--;
    }

    public void afiseazaListaElemente() {
        System.out.print("[");
        parcurge(new ActiuneElement() {
            public void actiune(Object e) {
                System.out.print(e + " ");
            }
        });
        System.out.println("]");
    }

    public void parcurge(ActiuneElement actiune) {
        Element item = head;
        while(item != null) {
            actiune.actiune(item.data);
            item = item.next;
        }
    }

    public void ataseaza(Lista sursa) {
        Lista destinatie = this;
        sursa.parcurge(new ActiuneElement() {
            public void actiune(Object e) {
                destinatie.adaugaElement(e);
            }
        });
    }

    //neterminat
    public void ataseaza(Lista sursa, int index) {
        if(index > size || index < 0) {
            throw new ArrayIndexOutOfBoundsException();
        }
        if(sursa.isEmpty()) {
            return;
        }
        if(index == size) {
            ataseaza(sursa);
            return;
        }
        Element current;
        if(index == 0) {
            current = head;
        } else {
            current = getInsideElement(index);
        }
        sursa.parcurge(new ActiuneElement() {
            public void actiune(Object e) {
                Element item = new Element(e, null, null);
                if(current.prev != null) {
                    current.prev.next = item;
                    item.prev = current.prev;
                }
                current.prev = item;
                item.next = current;
                size++;
            }
        });
        while(head.prev != null) {
            head = head.prev;
        }
        while(tail.next != null) {
            tail = tail.next;
        }
    }

    public Lista filtreaza(PredicatElement predicat) {
        LinkedList rez = new LinkedList();
        parcurge(new ActiuneElement() {
            public void actiune(Object e) {
                if(predicat.propozitie(e)) {
                    rez.adaugaElement(e);
                }
            }
        });
        return rez;
    }

    public Object get(int index) {
        if(index >= size || index < 0) {
            throw new ArrayIndexOutOfBoundsException();
        }
        if(index == (size - 1)) {
            return tail.data;
        }
        if(index == 0) {
            return head.data;
        }
        return getInsideElement(index).data;
    }
    private Element getInsideElement(int index) {
        //presupun ca index >= 0 && index < size && size > 0
        int i;
        Element item;
        if(index < (size / 2)) {
            i = 0;
            item = head;
            while(i < index) {
                item = item.next;
                i++;
            }
        } else {
            i = size - 1;
            item = tail;
            while(i > index) {
                item = item.prev;
                i--;
            }
        }
        return item;
    }

    public int size() {
        return size;
    }

    public boolean isEmpty() {
        return size == 0;
    }
}
