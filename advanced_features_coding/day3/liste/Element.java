package advanced_features_coding.day3.liste;

public class Element {
    public Object data;
    public Element next;
    public Element prev;
    public Element(Object data, Element next, Element prev) {
        this.data = data;
        this.next = next;
        this.prev = prev;
    }
}
