package advanced_features_coding.day3;

import advanced_features_coding.day3.ordonate.Coada;
import advanced_features_coding.day3.ordonate.Stiva;
import advanced_features_coding.day3.ordonate.StructuraOrdonata;

public class Exercitiu4_5 {
    public static void main(String[] args) {
        StructuraOrdonata s = new Stiva();
        for(int i = 0; i < 5; ++i) {
            s.push(new Integer(i));
        }
        while(!s.isEmpty()) {
            System.out.println(s.pop());
        }
    }
}
