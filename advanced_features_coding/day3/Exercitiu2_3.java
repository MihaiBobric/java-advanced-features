package advanced_features_coding.day3;

import advanced_features_coding.day3.liste.DynamicArray;
import advanced_features_coding.day3.liste.LinkedList;
import advanced_features_coding.day3.liste.Lista;
import advanced_features_coding.day3.generics.PredicatElement;

public class Exercitiu2_3 {
    public static void main(String[] args) {
        Lista l = new DynamicArray(); //cream o noua lista
        //pentru exercitiul 2:
        //Lista l = new DynamicArray();
        //pentru exercitiul 3:
        //Lista l = new LinkedList();
        System.out.println(l.isEmpty()); //true
        System.out.println(l.size()); //0
        l.adaugaElement(new Integer(5));//in lista avem: [5]
        System.out.println(l.isEmpty());//false
        System.out.println(l.size());//1
        l.adaugaElement(new Integer(6));//in lista avem: [5, 6]
        l.adaugaElement(new Integer(-1));//in lista avem: [5, 6, -1]
        l.afiseazaListaElemente();//[5 6 -1]
        //in lista pozitive vreau doar elementele [5 6]
        Lista pozitive = l.filtreaza(new PredicatElement() {
            public boolean propozitie(Object e) {
                if(((Integer)e).intValue() > 0) {
                    return true;
                }
                return false;
            }
        });
        pozitive.afiseazaListaElemente();//[5 6]
        l.stergeElement(1);//in lista avem: [5, -1]
        l.afiseazaListaElemente();//[5 -1]
        System.out.println(l.areElement(new Integer(-1)));//true
        while(!l.isEmpty()) {//stergem tot din lista
            l.stergeElement(0);
        }
        for(int i = 10; i < 15; ++i) {
            l.adaugaElement(new Integer(i), 0);//adaugam [14 13 12 11 10]
        }
        l.adaugaElement(new Integer(5));//adaugam 5
        l.afiseazaListaElemente();//[14 13 12 11 10 5]
        System.out.println(l.areElement(new Integer(12)));//true
        l.stergeElement(new Integer(12));//il sterg pe 12
        System.out.println(l.areElement(new Integer(12)));//false
        System.out.println(l.size());//5
        l.ataseaza(pozitive);//adaug la final [5 6]
        System.out.println(l.size());//7
        l.afiseazaListaElemente();//[14 13 11 10 5 5 6]
        l.ataseaza(pozitive, 0);//adaug la inceput [5 6]
        System.out.println(l.size());//9
        l.afiseazaListaElemente();//[5 6 14 13 11 10 5 5 6]
        l.ataseaza(pozitive, l.size() / 2);//adaug la mijloc [5 6]
        System.out.println(l.size());//11
        l.afiseazaListaElemente();//[5 6 14 13 5 6 11 10 5 5 6]
        l.ataseaza(pozitive, l.size());//adaug la final [5 6]
        System.out.println(l.size());//13
        l.afiseazaListaElemente();//[5 6 14 13 5 6 11 10 5 5 6 5 6]
        l.ataseaza(pozitive, l.size() - 1);//adaug aproape la final
        System.out.println(l.size());//15
        l.afiseazaListaElemente();//[5 6 14 13 5 6 11 10 5 5 6 5 5 6 6]
        System.out.println(l.get(0));//5
        System.out.println(l.get(l.size()/2));//10
        System.out.println(l.get(l.size() - 1));//6
    }
}
